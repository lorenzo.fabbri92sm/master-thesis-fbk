"""
Some useful (general) functions for my thesis. For instance, functions to create datasets.
"""

import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder

import pubchempy as pcp

PATH_DATA = '../../data/camda19/'
cell_lines_unique = ['A375', 'A549', 'HA1E', 'HCC515', 'HEPG2', 
                     'HT29', 'MCF7', 'PC3', 'VCAP', 'SW480', 'ASC', 
                     'NEU', 'NPC', 'PHH', 'SKB', 'BT20', 'HS578T', 
                     'MCF10A', 'MDAMB231', 'SKBR3', 'FIBRNPC', 
                     'NKDBA', 'HEK293T']

#==================================================#
def info_dataset(dataset):
    """
    Print some useful information about given dataset, 
    e.g. shape and number of NaN values.
    """
    
    print(f'Shape of dataset: {dataset.shape}.')
    print(f'Number of NaN values: {dataset.isna().sum().sum()}.\n')
    display(dataset.head())

def load_expr_matrix_csv(path_file, name):
    """
    Simply load gene expression matrix (CSV file) w/o modifying it.
    """
    
    # First we load a few rows to be able to change columns' types
    # to int32 to save memory
    expr = pd.read_csv(path_file, nrows=10)
    
    dtypes_old = expr.dtypes.to_dict()
    dtypes_new = dict()
    for el in dtypes_old:
        dtype = str(dtypes_old[el])
        dtype = dtype[:-2] + '32'
        dtypes_new[el] = dtype
    
    # Now we read again complete expression matrix
    expr32 = pd.read_csv(PATH_DATA+'expr/expr.csv', 
                        low_memory=True, dtype=dtypes_new)
    info_dataset(expr32)
    
    # Save expression matrix to feather format
    expr32.to_feather(PATH_DATA+f'expr/{name}')

def generate_expr_matrix_lm(dataset, path_gene_info, name):
    """
    Generate gene expression matrix w/ only landmark genes.
    """
    
    # Load gene information
    gene_info = pd.read_csv(path_gene_info, sep='\t', dtype=str)
    gene_info['pr_gene_id'] = gene_info['pr_gene_id'].astype('int32')
    gene_info['pr_is_lm'] = gene_info['pr_is_lm'].astype('int32')
    
    # Sub-select only genes w/ pr_is_lm=1 (landmark)
    expr32_f_lm = dataset.merge(gene_info[['pr_gene_id', 'pr_is_lm']], 
                                left_on='Unnamed: 0', right_on='pr_gene_id')
    expr32_f_lm = expr32_f_lm[expr32_f_lm.pr_is_lm==1]
    expr32_f_lm.drop(['pr_gene_id', 'pr_is_lm'], 
                     axis=1, inplace=True)
    assert(expr32_f_lm.shape[0]==978)  # Number of landmark genes
    info_dataset(expr32_f_lm)
    
    # Save to file
    expr32_f_lm.to_feather(PATH_DATA+f'expr/{name}')

def generate_sig_info(dataset, path_sig_info, name):
    """
    Retrieves information from sig_info file relevant to given dataset.
    """
    
    # Load sig_info file
    sig = pd.read_csv(path_sig_info, sep='\t')
    info_dataset(sig)
    print('==================================================')
    
    # Generate dictionary with information from sig_info file relevant to
    # provided dataset
    dict_info = dict()
    for coln in dataset.columns[1:]:
        dict_info[coln] = sig[sig.sig_id==coln].values.tolist()[0]
    
    # Create DataFrame from dictionary
    info_expr = pd.DataFrame.from_dict(dict_info, orient='index')
    info_expr.columns = ['sample', 'pert_id', 'pert_name', 'pert_type', 
                         'cell_line', 'dose', 'unit_dose', 'dose_unit', 
                         'time', 'unit_time', 'time_unit', 'distil_id']
    info_expr.drop(['sample', 'dose', 'unit_dose', 
                    'time', 'unit_time', 'distil_id'], 
                   axis=1, inplace=True)
    info_dataset(info_expr)
    
    # Save to file
    info_expr.to_csv(PATH_DATA+f'{name}.csv')

def generate_data_ml(expr, labels, name, info, 
                     cell_lines=None, compounds=None):
    """
    Generate dataset for supervised learning. Possibility to filter 
    gene expression matrix by cell lines and/or compounds.
    """
    
    info.rename(columns={'Unnamed: 0': 'full_id'}, 
               inplace=True)
    
    # Transpose expression matrix to have samples as rows and genes as columns
    data_ml = expr.copy().transpose()
    data_ml.rename(columns=expr.iloc[:, 0], 
                  inplace=True)
    data_ml.drop('Unnamed: 0', 
                 axis=0, inplace=True)
    
    # Insert column with name compounds
    comp_names = []
    for idx in data_ml.index:
        comp_name = info[info.full_id==idx]['pert_name'].values[0]
        comp_names.append(comp_name)
    data_ml.insert(0, 'compound', comp_names)
    
    # Insert labels for compounds
    labels_tmp = []
    for idx in range(len(data_ml.index)):
        comp_name = data_ml.at[idx, 'compound']
        
        try:
            label_tmp = labels[labels['Compound.Name']==comp_name]['label'].values[0]
        except:
            label_tmp = -1
        
        labels_tmp.append(label_tmp)
    data_ml.insert(1, 'vDili', labels_tmp)
    
    data_ml.reset_index(inplace=True)
    data_ml.rename(columns={'index': 'full_id'}, 
                   inplace=True)
    
    # Filter by cell line
    if cell_lines:
        cell_line = data_ml.merge(info[['full_id', 'cell_line']], 
                                  on='full_id').cell_line
        data_ml.insert(2, 'cell_line', cell_line)
        
        data_ml = data_ml[data_ml['cell_line'].isin(cell_lines)]
    
    # Filter by compound
    if compounds:
        data_ml = data_ml[data_ml['compound'].isin(compounds)]
    
    data_ml.reset_index(inplace=True, drop=True)
    info_dataset(data_ml)
    
    # Save to file
    data_ml.columns = data_ml.columns.astype(str)
    data_ml.to_feather(PATH_DATA+f'expr/{name}')
    
#==================================================#
def collapse_compounds(dataset):
    """
    Since for each cell line, it is possible that the same 
    compound is present multiple times, this function simply 
    collapses, for each cell line present in the dataset, 
    the rows based on compounds.
    """
    
    # Group by both cell line and compound
    gb = dataset.groupby(['cell_line', 'compound']).mean()
    gb.reset_index(inplace=True)
    
    return gb

#==================================================#
def get_smiles(data):
    """
    Retrieves SMILES for all the compounds in data given the name of the compound.
    """
    
    # List of unique compounds
    compounds = list(data['compound'].unique())
    # Dictionary mapping compounds to SMILES
    dict_compounds = dict()
    
    # Get molecules from PubChem using name
    for compound in compounds:
        print(f'Retrieving {compound}...', end='\t')
        c = pcp.get_compounds(compound, 'name')
        
        try:
            c = c[0]
            dict_compounds[compound] = c.canonical_smiles
        except:
            dict_compounds[compound] = -1
    
    return dict_compounds
