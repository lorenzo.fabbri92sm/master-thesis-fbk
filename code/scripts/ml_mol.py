"""
Class for DILI predictions using molecular descriptors and/or topological 
fingerprints.

Author: Lorenzo Fabbri
"""

"""
Possible extensions:
    - Multiprocessing for computing molecular descriptors -> https://github.com/rdkit/rdkit/issues/2529
"""

### Imports ###
import sys
from typing import List, Callable, Dict
import multiprocessing as mp
#import pathos
import itertools
import functools
import ast
import subprocess
import collections

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import pubchempy as pcp
import rdkit
from rdkit.Chem import Descriptors
from rdkit.Chem import AllChem
from rdkit.Chem import MACCSkeys
import pybel
sys.path.append('~/application/standardizer')
from standardiser import standardise
from cddd.inference import InferenceModel
from cddd.preprocessing import preprocess_smiles
import sygma

import sklearn.preprocessing
import sklearn.feature_selection
from sklearn.decomposition import PCA

FAIL = -999

### Utils ###
def concat_swissadme(list_path_to_csvs):
    """
    Since the server does not work when 
    given as input all SMILES from DILIrank, 
    this function merges all the partial CSV files.
    """
    
    list_dfs = []
    total_len = 0
    for idx, file in enumerate(list_path_to_csvs):
        list_dfs.append(pd.read_csv(file, sep=','))
        total_len += list_dfs[idx].shape[0]
    
    # Concat
    swissadme = pd.concat(list_dfs)
    swissadme.rename(columns={'Canonical SMILES': 'smiles'}, inplace=True)
    swissadme.reset_index(inplace=True, drop=True)
    
    # Convert to Canonical SMILES for safety
    for idx in swissadme.index:
        smi = swissadme.at[idx, 'smiles']
        swissadme.at[idx, 'smiles'] = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(smi), 
                                                            allBondsExplicit=True)
    
    assert swissadme.shape[0] == total_len
    
    # Save to file
    swissadme.to_csv('../../data/DILIrank/swissadme.csv', index=False)

### Class ###
class DILIMol:
    
    def __init__(self, data: pd.DataFrame, is_multitask: bool = False) -> None:
        """
        Initialize class with Pandas DataFrame. It should contain 
        the following columns (with these names):
        - id -> full ID from the CMAP data (optional)
        - compound -> name of the compound (e.g., aspirin)
        - cell_line -> ID of the cell line (optional)
        - label -> DILI class.
        """
        
        self.data = data
        self.is_multitask = is_multitask
        
        # Check required columns are present
        assert 'compound' in self.data.columns
        assert 'label' in self.data.columns
        
        # Store input columns
        self.columns = self.data.columns
    
    def get_smiles(self, data: pd.DataFrame, 
                   use_file: bool = True, path: str = None) -> pd.DataFrame:
        """
        Function to either retrieve the SMILES codes from 
        PubChem using PubChemPy or load them from file.
        """
        
        if use_file:  # Load from file
            
            df_tmp = pd.read_csv(path, header=None)
            df_tmp.columns = ['compound', 'smiles']
        
        else:  # Retrieve from PubChem
            
            # Create dictionary (compound: SMILES)
            compounds = data['compounds'].unique().tolist()
            smiles = {name: None for name in compounds}
            
            not_found = 0
            for compound in compounds:
                molecule = pcp.get_compounds(compound, 'name')
                
                try:
                    molecule = molecule[0]
                    smiles[compound] = molecule.canonical_smiles
                except:
                    not_found += 1
            
            if not_found != 0:
                print(f'Number of compounds not found: {not_found}.')
        
            # Create temporary DataFrame with SMILES codes
            df_tmp = pd.DataFrame(list(smiles.items()))
            df_tmp.columns = ['compound', 'smiles']
        
        # Merge DataFrames
        data_old_shape = data.shape[0]
        data = data.merge(df_tmp, on='compound')
        
        assert data_old_shape == data.shape[0]
        
        # Update input columns
        self.columns = data.columns
        
        return data
    
    def label_encode(self, data: pd.DataFrame, 
                    plot: bool = False) -> pd.DataFrame:
        """
        Encodes labels dataset.
        """
        
        # Encoder
        le = sklearn.preprocessing.LabelEncoder()
        labels_old = data['label']
        labels_new = le.fit_transform(data['label'])
        
        data['label'] = labels_new
        
        # Count-plot
        if plot:
            
            fig, ax = plt.subplots(2, 1, figsize=(15, 9))
            
            # Count-plot original labels
            sns.countplot(labels_old, ax=ax[0])
            
            # Count-plot encoded labels
            sns.countplot(data['label'], ax=ax[1])
            
            plt.show()
        
        assert labels_old.shape[0] == data.shape[0]
        
        # Update input columns
        self.columns = data.columns

        # Print mapping label-integer
        mapping = dict(zip(le.classes_, le.transform(le.classes_)))
        print(mapping)
        
        return data, mapping
    
    def to_canonical(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Converts SMILES codes in input DataFrame to canonical SMILES codes. 
        DataFrame must contain column 'smiles'.
        """

        data.reset_index(inplace=True, drop=True)
        for idx in data.index:
            smi = data.at[idx, 'smiles']
            data.at[idx, 'smiles'] = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(smi), 
                                                            allBondsExplicit=True)
        data.reset_index(inplace=True, drop=True)
        
        return data
    
    def generate_descriptors(self, data: pd.DataFrame, 
                            use_mol_descriptors: bool, 
                            use_top_fingerprints: bool, 
                            type_fps: str, 
                            use_cddd: bool, use_gpu: bool, 
                            use_file_cddd: bool, path_in_cddd: str, path_out_cddd: str, 
                            use_additional_descriptors: bool, 
                            use_swissadme: bool, path_swissadme: str, 
                            use_stitch_gtex: bool, use_networks: bool, 
                            use_metabolism: bool) -> pd.DataFrame:
        """
        Generates molecular descriptors and/or topological 
        fingerprints using RDKit (mainly).
        """

        assert 'smiles' in data.columns
        data_old_shape = data.shape
        
        # Convert to Canonical SMILES for safety
        data = self.to_canonical(data)

        # Unique compounds (based on SMILES codes)
        assert len(data['smiles'].unique().tolist()) == \
                len(data['smiles'].tolist())
        smiles = data['smiles'].unique().tolist()
        assert smiles == data['smiles'].tolist()
        
        ## Generate molecular descriptors
        if use_mol_descriptors:
            # Dictionary to store DataFrames with descriptors
            dict_mol_descriptors = dict()
            
            mol_descriptors = [desc for desc in Descriptors.descList]
            
            # Iterate over all the molecular descriptors
            for descriptor in mol_descriptors:
                desc_all_smiles = self.generate_mol_descriptor(smiles, 
                                                              descriptor)
                
                # Add to dictionary
                df_tmp = pd.DataFrame(list(desc_all_smiles.items()))
                df_tmp.columns = ['smiles', f'{descriptor[0]}']
                dict_mol_descriptors[descriptor[0]] = df_tmp
            
            # Merge DataFrames
            merge = functools.partial(pd.merge, 
                                      on='smiles', how='outer')
            merged_dfs_mol_desc = functools.reduce(merge, 
                                                   dict_mol_descriptors.values())
            dict_mol_descriptors = None
        
        ## Additional descriptors from RDKit and Pybel
        if use_additional_descriptors:

            # Helper functions to compute several filters (rules)
            def filters(molecule: rdkit.Chem.MolFromSmiles, smiles: str) -> dict:
                mw = rdkit.Chem.Descriptors.ExactMolWt(mol)
                logp = rdkit.Chem.Crippen.MolLogP(mol)
                atom_count = molecule.GetNumAtoms()
                mr = pybel.readstring('smi', smiles).calcdesc()['MR']
                hbond_donor = rdkit.Chem.Lipinski.NumHDonors(mol)
                hbond_acc = rdkit.Chem.Lipinski.NumHAcceptors(mol)
                rot_bound = rdkit.Chem.rdMolDescriptors.CalcNumRotatableBonds(mol)
                tpsa = rdkit.Chem.rdMolDescriptors.CalcTPSA(mol)

                ghose, lipinski, rule3, veber = 0, 0, 0, 0

                # Ghose filter (extrema included?)
                if (160 <= mw <= 480) and (-0.4 <= logp <= 5.6) and (20 <= atom_count <= 70) and (40 <= mr <= 130):
                    ghose = 1

                # Lipinski filter
                if (mw <= 500) and (logp <= 5.0) and (hbond_donor <= 5) and (hbond_acc <= 10):
                    lipinski = 1
                
                # Rule of 3
                if (mw <= 300) and (logp <= 3) and (hbond_donor <= 3) and (hbond_acc <= 3) and (rot_bound <= 3):
                    rule3 = 1
                
                # Veber filter
                if (rot_bound <= 10) and (tpsa <= 140):
                    veber = 1
                
                all_filters = {'ghose': ghose, 'lipinski': lipinski, 
                                'rule3': rule3, 'veber': veber}
                
                return all_filters

            dict_all_descs_unique_smiles = collections.defaultdict(dict)

            # Iterate over unique SMILES codes
            for smi in smiles:
                mol = rdkit.Chem.MolFromSmiles(smi)

                # Compute additional descriptors
                sp3 = rdkit.Chem.rdMolDescriptors.CalcFractionCSP3(mol)
                all_filters = filters(mol, smi)

                # Store additional descriptors
                dict_all_descs_unique_smiles[smi]['fsp3'] = sp3
                dict_all_descs_unique_smiles[smi]['ghose'] = all_filters['ghose']
                dict_all_descs_unique_smiles[smi]['lipinski'] = all_filters['lipinski']
                dict_all_descs_unique_smiles[smi]['rule3'] = all_filters['rule3']
                dict_all_descs_unique_smiles[smi]['veber'] = all_filters['veber']
            
            # Create DataFrame for later merging based on SMILES codes
            additional_mol_desc = pd.DataFrame.from_dict(dict_all_descs_unique_smiles, orient='index')
            additional_mol_desc.reset_index(inplace=True)
            additional_mol_desc.rename(columns={'index': 'smiles'}, inplace=True)
        
        ## SwissADME
        if use_swissadme:

            # File with descriptors for all compounds. Use website
            #concat_swissadme([f'../../data/DILIrank/swissadme_{i}.csv' for i in range(1, 5)])
            
            swissadme = pd.read_csv(path_swissadme, sep=',')
            swissadme.drop(['Molecule', 'Formula'], axis=1, inplace=True)

            colns_to_encode = swissadme.loc[:, ((swissadme.dtypes != np.float) & (swissadme.dtypes != np.int))].columns.tolist()
            colns_to_encode.pop(0)  # Do not consider SMILES codes
            for col in colns_to_encode:
                le = sklearn.preprocessing.LabelEncoder()
                new_labels = le.fit_transform(swissadme[col])
                swissadme[col] = new_labels
        
        ## Stitch + GTEx (genes)
        if use_stitch_gtex:

            # Each row is a compound. Columns are SMILES codes and all target genes
            stitch_gtex = pd.read_csv('../../data/DILIrank/stitch_gtex.csv')
            stitch_gtex.columns = ['gtex_'+col for col in stitch_gtex.columns[:-1]] + ['smiles']

            # Filter features (genes) based on std or PCA
            threshold = None  # 0.95 or None
            pca = False
            n_components = 3

            if threshold is not None:
                l = stitch_gtex.shape[0]  # All compounds
                to_drop = []  # Columns to drop since too many zeros
                for col in stitch_gtex.columns.tolist()[:-1]:
                    val = stitch_gtex[stitch_gtex[col] == 0.0].shape[0] / l
                    if val > threshold:
                        to_drop.append(col)
                stitch_gtex.drop(to_drop, axis=1, inplace=True)
            
            if pca:
                red = PCA(n_components=n_components)
                smiles = stitch_gtex['smiles']
                genes = stitch_gtex.columns.tolist()[:-1]
                transformed = red.fit_transform(stitch_gtex.drop('smiles', axis=1))

                stitch_gtex = pd.concat([smiles, pd.DataFrame(transformed)], axis=1)
                assert transformed.shape[1] == n_components
                assert transformed.shape[0] == stitch_gtex.shape[0]
        
        ## Properties networks as found by STITCH
        if use_networks:

            nets = pd.read_csv('../../data/DILIrank/net_feats.csv')
        
        ## For each SMILES code, predict most probable
        ## metabolite and compute relevant descriptors
        if use_metabolism:
            res_met = {}
            
            for idx, smi in enumerate(smiles):
                # Predict metabolite
                smi_met = self.predict_metabolite(smi)
                mol = rdkit.Chem.MolFromSmiles(smi_met)
                
                # Compute descriptors (Fsp3 + cLogP) for metabolite and store them
                sp3 = rdkit.Chem.rdMolDescriptors.CalcFractionCSP3(mol)
                logP = rdkit.Chem.Crippen.MolLogP(mol)
                res_met[smi] = {'smiles': smi_met, 'fsp3_met': sp3, 'clogp_met': logP}
            
            # Merge into unique DataFrame
            metabolism = pd.DataFrame.from_dict(res_met, orient='index')
            metabolism.reset_index(inplace=True)
            metabolism.columns = ['smiles', 'smiles_met', 'fsp3_met', 'clogp_met']
            metabolism.drop(['smiles_met'], axis=1, inplace=True)
        
        ## Generate CDD descriptors
        if use_cddd:
            
            data['smiles'].to_csv(path_in_cddd, index=False, header=['smiles'])
            
            if use_file_cddd is not True:
                # Pre-processing and encoding
                model_dir_cddd = "../../applications/cddd/default_model/"
                command = f'cddd --input {path_in_cddd} --output {path_out_cddd} --model_dir {model_dir_cddd}'
                command = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
                output = command.communicate()[0]
                print(output)
            
            # Create correct DataFrame w/
            # descriptors for merging
            smiles_embedding = pd.read_csv(path_out_cddd)
            smiles_embedding.drop(['Unnamed: 0', 'new_smiles'], 
                                  axis=1, inplace=True)
            
            dropped_mols = smiles_embedding['cddd_1'].isna().sum().sum()
            smiles_embedding.dropna(inplace=True)
            smiles_embedding.reset_index(inplace=True, drop=True)
       
        ## Generate topological fingerprints
        if use_top_fingerprints:
            
            # Dictionary to store fingerprints
            dict_top_fingerprints = dict()
            
            # Iterate over all the SMILES codes
            for smi in smiles:
                
                fps = self.generate_top_fingerprints(smi, type_fps=type_fps)
                
                dict_top_fingerprints[smi] = fps
            
            # Create DataFrame with SMILES codes and fingerprints
            df_tmp = pd.DataFrame(list(dict_top_fingerprints.items()))
            df_tmp.columns = ['smiles', f'fps_{type_fps}']
            
            # Convert list fps to columns
            def convert_fps(data, column):
                
                """# Convert '[]' to []
                for idx in data.index:
                    which = data.loc[data.index[idx], column]
                    data.at[idx, column] = ast.literal_eval(which)"""
                    
                # Convert [] to columns
                fps = pd.DataFrame(data[column].values.tolist(), 
                                  index=data.index)
                fps.columns = [column+f'_{i}' for i in range (fps.shape[1])]
                
                data = pd.concat([data.drop(column, axis=1), fps], 
                                axis=1)
                
                return data
            
            df_tmp = convert_fps(df_tmp, f'fps_{type_fps}')
        
        # Merge DataFrames
        merge = functools.partial(pd.merge, 
                                  on='smiles', how='inner')

        dfs_to_merge = [data]
        if use_mol_descriptors:
            dfs_to_merge.append(self.to_canonical(merged_dfs_mol_desc))
        if use_additional_descriptors:
            dfs_to_merge.append(self.to_canonical(additional_mol_desc))
        if use_swissadme:
            dfs_to_merge.append(self.to_canonical(swissadme))
        if use_stitch_gtex:
            dfs_to_merge.append(self.to_canonical(stitch_gtex))
        if use_networks:
            dfs_to_merge.append(self.to_canonical(nets))
        if use_cddd:
            dfs_to_merge.append(self.to_canonical(smiles_embedding))
        if use_top_fingerprints:
            dfs_to_merge.append(self.to_canonical(df_tmp))
        if use_metabolism:
            dfs_to_merge.append(self.to_canonical(metabolism))

        data = functools.reduce(merge, dfs_to_merge)
        dict_top_fingerprints = None
        
        # Clean-up
        data.reset_index(inplace=True, drop=True)
        
        """if use_cddd:
            assert data_old_shape[0] - dropped_mols == data.shape[0]
        elif not use_cddd and not use_stitch_gtex:
            assert data_old_shape[0] == data.shape[0]"""
        
        # Feature selection
        data_old_shape = data.shape
        data = self.feature_selection(data, variance_zero=True, 
                                    variance_low=False, variance_threshold=0.5, 
                                    nanNull_columns=True)
        assert data.shape[0] == data_old_shape[0]
        
        return data
    
    def predict_metabolite(self, smiles: str) -> str:
        """
        Predict for input SMILES code the most probable 
        metabolite using Sygma.
        """
        
        try:
            scenario = sygma.Scenario([
                [sygma.ruleset['phase1'], 1], 
                [sygma.ruleset['phase2'], 1]]
            )

            parent = rdkit.Chem.MolFromSmiles(smiles)

            metabolic_tree = scenario.run(parent)
            metabolic_tree.calc_scores()
            smi_met = metabolic_tree.to_smiles()[1][0]
            
        except:
            # In case no metabolite predicted, returns original compound
            smi_met = smiles
        
        return smi_met
    
    def generate_mol_descriptor(self, smiles: List[str], 
                               mol_descriptor: Callable) -> Dict:
        """
        Generates one molecular descriptor for all compounds 
        (as SMILES codes).
        """
        
        # Store results in dictionary
        desc_all_smiles_dict = {smi: None for smi in smiles}
        
        # Iterate over all the SMILES codes
        for smi in smiles:
            
            try:
                molecule = rdkit.Chem.MolFromSmiles(smi)
                desc = mol_descriptor[1](molecule)
                
                desc_all_smiles_dict[smi] = desc
                
            except Exception as e:
                desc_all_smiles_dict[smi] = FAIL
        
        return desc_all_smiles_dict
    
    def generate_top_fingerprints(self, smiles: str, 
                                 type_fps: str) -> List:
        """
        Generates topological fingerprints (one type 
        among those that are available) for one 
        compound (as SMILES code).
        """
        
        assert type_fps in ['morgan', 'maccs', 'obabel']
        
        # Morgan
        radius = 5
        n_bits_morgan = 2048
        # OpenBabel
        ob_type = 'FP4'
        assert ob_type in ['FP2', 'FP3', 'FP4']
        
        molecule = rdkit.Chem.MolFromSmiles(smiles)
        
        if type_fps == 'morgan':
            fps = AllChem.GetMorganFingerprintAsBitVect(molecule, 
                                                        radius, n_bits_morgan)
            fps = list(fps)
            
            assert len(fps) == n_bits_morgan
        
        elif type_fps == 'maccs':
            fps = MACCSkeys.GenMACCSKeys(molecule)
            fps = list(fps)[1:]
            
            assert len(fps) == 166
        
        elif type_fps == 'obabel':
            molecule_ob = pybel.readstring('smi', smiles)
            
            fps = molecule_ob.calcfp(ob_type)
            tmp = np.zeros(max(fps.bits)+1)
            tmp[fps.bits] = 1
            fps = list(tmp)
        
        return fps
    
    def preprocess_descriptors(self, 
                               data_train: pd.DataFrame, 
                               data_val: pd.DataFrame, 
                               method: str) -> pd.DataFrame:
        """
        Applies standardization or normalization techniques 
        to specified descriptors. Fitting is performed on 
        training set only.
        
        Methods:
            - Scaling
                * 'StandardScaler' -> zero mean and unit variance
                * 'MinMaxScaler' -> [0, 1] range for the features
            - Normalization
                * 'Normalize' -> scaling individual samples to unit norm.
        """
        
        def drop_fps(data):
            regex = list(data.filter(regex='fps'))
            fps = data[regex]
            data = data[data.columns.drop(regex)]
            
            return data, fps
        
        def add_fps(data, fps):
            data = pd.concat([data, fps], 
                             axis=1)
            
            return data
        
        ## Drop columns
        assert 'label' not in data_train.columns
        assert 'label' not in data_val.columns
        original_shape_train = data_train.shape
        original_shape_val = data_val.shape
        
        # Fingerprints
        data_train, fps_train = drop_fps(data_train)
        data_val, fps_val = drop_fps(data_val)
        
        name_colns = data_train.columns.tolist()
        assert name_colns == data_val.columns.tolist()
        
        ## Pre-processing
        accepted_methods = ['StandardScaler', 'MinMaxScaler', 
                           'Normalize']
        assert method in accepted_methods
        
        method = eval(f'sklearn.preprocessing.{method}()')
        
        # Fit
        method.fit(data_train.values)
        # Transform
        data_train = method.transform(data_train.values)
        data_val = method.transform(data_val.values)
        
        ## Concatenate data (processed descriptors + fps)
        # Add column names
        data_train = pd.DataFrame(data_train, 
                                  columns=name_colns)
        data_val = pd.DataFrame(data_val, 
                                columns=name_colns)
        
        # Concatenate fingerprints
        data_train = add_fps(data_train, fps_train)
        data_val = add_fps(data_val, fps_val)
        
        assert original_shape_train == data_train.shape
        assert original_shape_val == data_val.shape
        assert 'label' not in data_train.columns
        assert 'label' not in data_val.columns
        
        return data_train, data_val
    
    def feature_selection(self, data: pd.DataFrame, 
                        variance_zero: bool, 
                        variance_low: bool, 
                        variance_threshold: float, 
                        nanNull_columns: bool) -> pd.DataFrame:
        """
        Performs feature selection.
        """
        
        #print(f'\nShape of current dataset: {data.shape}.')
        
        # Drop columns
        data_desc = data.drop(self.columns, axis=1).copy()
        
        # Check if any NaN or null values
        if nanNull_columns:
            nan = data_desc.isna().sum().sum()
            null = data_desc.isnull().sum().sum()
            fails = (data_desc == FAIL).sum().sum()
            
            #print(f'Number of missing values: {nan}.')
            #print(f'Number of null values: {null}.')
            #print(f'Number of molecular descriptors not computed: {fails}.')
        
        # Remove zero-variance columns
        if variance_zero:
            data_desc = data_desc.loc[:, data_desc.var() > 0.0].copy()
        
        # Remove low-variance columns
        if variance_low:
            data_desc = data_desc.loc[:, data_desc.var() > variance_threshold].copy()
        
        """# Drop FPS
        regex = list(data_desc.filter(regex='fps'))
        fps = data[regex]
        data_desc = data_desc[data_desc.columns.drop(regex)]"""
        
        # Concatenate DataFrame
        #assert num_colns_dropped+data_desc.shape[1]+len(self.columns) == data.shape[1]
        data = pd.concat([data[self.columns], 
                         data_desc], axis=1)
        
        # Remove descriptors containing NaN values
        if nanNull_columns:
            data.dropna(inplace=True, axis=1)
        
        #print(f'Shape of new dataset: {data.shape}.')
        #print(f'Number of molecular descriptors removed: {num_colns_dropped}.')
        
        return data
    
    def filter_data(self, data: pd.DataFrame, 
                    by: list) -> pd.DataFrame:
        """
        Removes duplicates: the length of the DataFrame should match 
        the number of unique elements in the column 'by'.
        """
        
        #print(f'Shape of current dataset: {data.shape}.')
        
        #new_length = len(data[by].unique())
        for coln in by:
            data = data.drop_duplicates(subset=coln)
        #assert data.shape[0] == new_length
        
        #print(f'Shape of new dataset: {data.shape}.')
        
        return data
    
    def chemotype_curation(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Performs curation on compounds. Adapted 
        from Kotsampasakou et al. (2017).
        """
        
        #print(f'Shape of current dataset: {data.shape}.')
        data.reset_index(inplace=True, drop=True)
        
        # Step 1: remove inorganic compounds
        def is_organic(smi: str) -> bool:
            
            is_organic = True if 'C' in smi else False
            
            return is_organic
        
        data = data[data.smiles.apply(is_organic) == True].copy()
        data.reset_index(inplace=True, drop=True)
        
        # Step 2: use Standardiser tool from Atkinson (2014)
        standardise.logger.setLevel(20)
        
        def smiles_after_curation(idx: int) -> str:
            
            smi = data.at[idx, 'smiles']
            mol = rdkit.Chem.MolFromSmiles(smi)
            
            new_mol = standardise.run(mol)
            new_smi = rdkit.Chem.MolToSmiles(new_mol, 
                                             canonical=True, 
                                             allBondsExplicit=True)
            
            return new_smi
        
        for idx in data.index:
            
            try:
                new_smi = smiles_after_curation(idx)
            except:
                new_smi = data.at[idx, 'smiles']

            data.at[idx, 'smiles'] = new_smi
        
        # Step 3: remove permanently charged compounds and
        # drop stereoisomers (keep one if same class)
        idxs_to_drop = []
        for idx in data.index:
            
            smi = data.at[idx, 'smiles']
            mol = rdkit.Chem.MolFromSmiles(smi)
            
            if rdkit.Chem.rdmolops.GetFormalCharge(mol) > 0:
                idxs_to_drop.append(idx)
        
        data.drop(idxs_to_drop, inplace=True)
        data.drop_duplicates(subset='smiles', inplace=True)
        data.reset_index(inplace=True, drop=True)
        
        #print(f'Shape of new dataset: {data.shape}.')
        
        return data
    
    def merge_labels(self, data: pd.DataFrame, 
                     labels_to_drop: list = None, 
                     labels_to_merge: dict = None) -> pd.DataFrame:
        """
        Gives possibility to merge and/or to drop 
        selected labels.
        """
        
        # Drop selected labels
        if labels_to_drop:
            data = data[~data.label.isin(labels_to_drop)]
        
        # Merge selected labels
        if labels_to_merge:
            for old, new in labels_to_merge.items():
                data['label'].replace(old, new, inplace=True)
        
        # If labels do not start at 0, subtract max
        assert -1 not in data.label.values
        if data['label'].min() != 0:
            label_min = data['label'].min()

            data.label = data.label - label_min
        
        return data
