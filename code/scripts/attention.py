import torch
from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim.lr_scheduler import CosineAnnealingLR, StepLR
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)
print('\n')

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import matthews_corrcoef, classification_report, confusion_matrix, recall_score

import pandas as pd
import feather
import numpy as np
from collections import defaultdict
import re

import matplotlib.pyplot as plt
import seaborn as sns

from rdkit import Chem
from rdkit.Chem.SaltRemover import SaltRemover

import custom_split as split
from plot_confusion_matrix import plot_confusion_matrix

def to_canonical(data):
    data.reset_index(inplace=True, drop=True)
    for idx in data.index:
        smi = data.at[idx, 'smiles']
        new_smi = Chem.MolToSmiles(Chem.MolFromSmiles(smi))
        data.at[idx, 'smiles'] = new_smi
    return data

def enumerate_smi(smi):
    mol = Chem.MolFromSmiles(smi)
    num_atoms = list(range(mol.GetNumAtoms()))
    np.random.shuffle(num_atoms)
    renumber_atoms = Chem.RenumberAtoms(mol, num_atoms)
    
    new_smi = Chem.MolToSmiles(renumber_atoms, canonical=False, 
                              isomericSmiles=True)
    return new_smi

def encode(mapping, smi, pad):
    encoded = np.zeros((pad+1), dtype=np.int8)
    for i, char in enumerate(smi):
        encoded[i] = mapping[char]

    return encoded

# The following helper functions are borrowed from the CDDD GitHub's repository
def keep_largest_fragment(smi):
    mol_frags = Chem.GetMolFrags(Chem.MolFromSmiles(smi), asMols=True)
    largest_mol = None
    largest_mol_size = 0

    for mol in mol_frags:
        size = mol.GetNumAtoms()
        if size > largest_mol_size:
            largest_mol = mol
            largest_mol_size = size

    return Chem.MolToSmiles(largest_mol)

def remove_salt_stereo(smi):
    remover = SaltRemover()
    smi = Chem.MolToSmiles(remover.StripMol(Chem.MolFromSmiles(smi), 
                                            dontRemoveEverything=True), 
                            isomericSmiles=False)
    if '.' in smi:
        smi = keep_largest_fragment(smi)
    
    return smi

def organic_filter(smi):
    ORGANIC_ATOM_SET = set([5, 6, 7, 8, 9, 15, 16, 17, 35, 53])
    m = Chem.MolFromSmiles(smi)
    atom_num_list = [atom.GetAtomicNum() for atom in m.GetAtoms()]
    is_organic = (set(atom_num_list) <= ORGANIC_ATOM_SET)

    if is_organic:
        return True
    else:
        return False

def char_to_idx(smi, pad):
    REGEX_SMI = r'Cl|Br|[#%\)\(\+\-1032547698:=@CBFIHONPS\[\]cionps]'
    path = '../../applications/cddd/cddd/data/indices_char.npy'
    vocabulary = {
        v: k for k, v in np.load(path, allow_pickle=True).item().items()
    }

    char_list = re.findall(REGEX_SMI, smi)
    encoded = np.array([vocabulary[char_list[j]] for j in range(len(char_list))]).astype(np.int32)
    encoded = np.pad(encoded, (0, pad-len(encoded)), 'constant', constant_values=(0,))
    
    return encoded, vocabulary

def main(which_splitter, train_size, threshold_len_smiles, enum_many, 
        num_classes, sampling, batch_size, 
        attention_size, num_epochs, lr, weight_decay, seed, 
        scheduling, mean_cm):
    
    ########## DATA ##########
    
    data = pd.read_excel('../../data/DILIrank/DILIrank.xls')
    data = data[['Compound Name', 'vDILIConcern', 'SMILES']]
    data.columns = ['compound', 'label', 'smiles']
    data = data[data.smiles != '.']
    data = data[data.label != 'Ambiguous DILI-concern']
    data = to_canonical(data)
    
    if num_classes == 2:
        data = data[data.label != 'vLess-DILI-Concern']
        most = 0
    else:
        most = 1
        no = 2
    
    ## Chemotype curation
    old_shape = data.shape

    # Largest fragment
    data['smiles'] = data['smiles'].apply(lambda x: keep_largest_fragment(x))

    # Remove stereochemistry information and strips salts
    data['smiles'] = data['smiles'].apply(lambda x: remove_salt_stereo(x))
    before_stereo = data.shape
    data.drop_duplicates(subset='smiles', inplace=True)
    print(f'Before stereochemistry: {before_stereo}. After stereochemistry: {data.shape}')

    # Filter out non organic molecules
    data = data[data['smiles'].apply(lambda x: organic_filter(x) == True)]
    data.dropna(subset=['smiles'], inplace=True)

    # Filter compounds by length SMILES codes
    data = data[data['smiles'].apply(lambda x: len(x) <= threshold_len_smiles)]

    print(f'Old shape: {old_shape}. New shape: {data.shape}\n')
    ##
    
    # Encode labels
    le = LabelEncoder()
    labs = le.fit_transform(data['label'])
    data['label'] = labs
    if num_classes == 3:
        print(le.inverse_transform([0, 1, 2]))
    else:
        print(le.inverse_transform([0, 1]))
    print('\n')
    
    # Splitting
    data.drop('compound', axis=1, inplace=True)

    if which_splitter is None:
        train, val = train_test_split(data, train_size=train_size, 
                                    shuffle=True, 
                                    random_state=seed)
    elif which_splitter == 'ScaffoldSplit':
        data = data.sample(frac=1.0, random_state=seed)
        data.reset_index(inplace=True, drop=True)

        splitter = eval(f'split.{which_splitter}(train_size={train_size}, seed={seed})')
        train_idxs, val_idxs = splitter.split(smiles=data['smiles'], X=data['smiles'], y=data['label'], 
                                            stratify=None)

        train, val = data.iloc[train_idxs, :], data.iloc[val_idxs, :]
    
    # Assert no leak compounds in splits
    tmp = len(set(train['smiles']) & set(val['smiles']))
    assert tmp == 0, 'Data leak during split.'

    """fig, ax = plt.subplots(1, 2, figsize=(15, 7))
    sns.countplot(le.inverse_transform(train.label), ax=ax[0]).set_title('Count-plot without over-sampling (training).')
    sns.countplot(le.inverse_transform(val.label), ax=ax[1]).set_title('Count-plot without over-sampling (validation).')
    plt.show()"""
    
    if sampling == 'sampler':
        class_sample_counts = train.groupby('label').count()['smiles'].values.tolist()
    elif sampling == 'duplicate':
        train = pd.concat([train, train[train.label == most].sample(frac=0.4)], 
                          axis=0, ignore_index=True)

        if num_classes == 3:
            train = pd.concat([train, train[train.label == no].sample(frac=0.05)], 
                            axis=0, ignore_index=True)
        
        """fig, ax = plt.subplots(1, 2, figsize=(15, 7))
        sns.countplot(le.inverse_transform(train.label), ax=ax[0]).set_title('Count-plot with over-sampling.')
        sns.countplot(le.inverse_transform(val.label), ax=ax[1]).set_title('Count-plot with over-sampling (validation).')
        plt.show()"""
    
    train.reset_index(inplace=True, drop=True)
    val.reset_index(inplace=True, drop=True)
    
    # SMILES enumeration
    if enum_many > 0:
        enumerated = {}
        pos = 0
        for idx in train.index:
            smi = train.at[idx, 'smiles']
            label = train.at[idx, 'label']

            for _ in range(enum_many):
                enum = enumerate_smi(smi)
                enumerated[pos] = [enum, label]
                pos += 1

        df_enum = pd.DataFrame.from_dict(enumerated, orient='index')
        df_enum.columns = ['smiles', 'label']
        df_enum = df_enum[['label', 'smiles']]

        train = pd.concat([train, df_enum], ignore_index=True, axis=0)
        train.reset_index(inplace=True, drop=True)
    
    # SMILES encoding for embedding
    train_charset = ''.join(train['smiles'].tolist())
    val_charset = ''.join(val['smiles'].tolist())
    charset = set(train_charset + val_charset)
    char_to_int = {c: i for i, c in enumerate(charset, 1)}
    
    pad_train = len(max(train['smiles'].tolist(), key=len))
    pad_val = len(max(val['smiles'].tolist(), key=len))
    pad = max(pad_train, pad_val)
    
    train.reset_index(inplace=True, drop=True)
    X_train = []
    y_train = []
    for idx in train.index:
        smi = train.at[idx, 'smiles']
        label = train.at[idx, 'label']
        #encoded_smi = encode(char_to_int, smi, pad)
        encoded_smi, mapping = char_to_idx(smi, pad)
        X_train.append(encoded_smi)
        y_train.append(label)

    val.reset_index(inplace=True, drop=True)
    X_val = []
    y_val = []
    for idx in val.index:
        smi = val.at[idx, 'smiles']
        label = val.at[idx, 'label']
        #encoded_smi = encode(char_to_int, smi, pad)
        encoded_smi, mapping = char_to_idx(smi, pad)
        X_val.append(encoded_smi)
        y_val.append(label)
    
    ########## DATASETS ##########
    
    class SmilesDataset(Dataset):
        
        def __init__(self, X, y):
            super(SmilesDataset, self).__init__()
            
            self.X = X
            self.y = y
            
        def __getitem__(self, idx):
            smi = self.X[idx]
            label = self.y[idx]
            
            return smi, label
        
        def __len__(self):
            return len(self.X)
    
    # Sampler for data imbalance
    if sampling == 'sampler':
        weights = 1. / torch.tensor(class_sample_counts, dtype=torch.float)
        samples_weights = weights[y_train]
        sampler = WeightedRandomSampler(weights=samples_weights, 
                                        num_samples=len(samples_weights), 
                                        replacement=True)
    
    train_ds = SmilesDataset(X_train, y_train)
    train_loader = DataLoader(train_ds, batch_size=batch_size, 
                              sampler=sampler if sampling=='sampler' else None, 
                              shuffle=False if sampling=='sampler' else True, num_workers=2)
    
    val_ds = SmilesDataset(X_val, y_val)
    val_loader = DataLoader(val_ds, batch_size=batch_size, 
                            shuffle=True, num_workers=2)
    
    ########## MODEL ##########
    
    class Attention(nn.Module):
        """
        From PaccMann (IBM Zurich).
        """
        
        def __init__(self, seq_len, embedding_dim, 
                     attention_size, num_classes, mode=None):
            super(Attention, self).__init__()

            self.mode = mode
            
            # Embedding
            self.embedding = nn.Embedding(seq_len, embedding_dim)
            self.hidden_size = embedding_dim
            
            # Trainable parameters for attention mechanism
            W_omega = torch.empty(self.hidden_size, attention_size).normal_(0, 0.1)
            b_omega = torch.empty(attention_size).normal_(0, 0.1)
            u_omega = torch.empty(attention_size).normal_(0, 0.1)
            
            self.W_omega = W_omega.to(device)
            self.b_omega = b_omega.to(device)
            self.u_omega = u_omega.to(device)
            
            # Classifier
            self.fc1 = nn.Linear(self.hidden_size, 10)
            self.fc2 = nn.Linear(10, 10)
            self.fc3 = nn.Linear(10, num_classes)
            self.drop = nn.Dropout(p=0.6)
            
            # Initialize weights
            nn.init.xavier_uniform_(self.embedding.weight)
            nn.init.xavier_uniform_(self.fc1.weight)
            nn.init.constant_(self.fc1.bias, 0)
            nn.init.xavier_uniform_(self.fc2.weight)
            nn.init.constant_(self.fc2.bias, 0)
            nn.init.xavier_uniform_(self.fc3.weight)
            nn.init.constant_(self.fc3.bias, 0)
        
        def forward(self, x):
            
            embedded = self.embedding(x.long())
            
            ## Attention mechanism
            # [batch_size, seq_len, attention_size]
            v = torch.tanh(torch.matmul(embedded, self.W_omega) + self.b_omega)
            
            # [batch_size, seq_len]
            vu = torch.matmul(v, self.u_omega)
            alphas = F.softmax(vu, dim=1)
            
            ## Reduce sequence
            # [batch_size, hidden_size]
            reduced = (embedded * alphas.unsqueeze(-1)).sum(dim=1)
            
            # Classifier
            output = F.relu(self.fc1(reduced))
            output = self.drop(output)
            #output = F.relu(self.fc2(output))
            #output = self.drop(output)
            output = self.fc3(output)

            if self.mode == 'explain':
                return output, alphas
            
            return output
    
    model = Attention(seq_len=pad, embedding_dim=16, 
                      attention_size=attention_size, 
                      num_classes=num_classes, mode='explain')
    model.to(device)
    
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
    if scheduling is not None:
        if scheduling == 'cosine':
            scheduler = CosineAnnealingLR(optimizer, T_max=20)
        elif scheduling == 'step':
            scheduler = StepLR(optimizer, step_size=25, gamma=0.1)
    
    ########## TRAINING ##########
    
    training_loss = []
    val_loss = []
    
    for epoch in range(1, num_epochs+1):
        #print(f'EPOCH: {epoch}...')

        model.train()

        avg_loss = 0.0
        for idx, batch in enumerate(train_loader):
            smiles, labels = batch[0].to(device), batch[1].to(device)

            # Fit
            optimizer.zero_grad()

            out, _ = model(smiles)

            loss = criterion(out, labels)
            avg_loss += loss.item()
            loss.backward()

            optimizer.step()
        training_loss.append(avg_loss / len(train_loader))

        # Validation
        model.eval()
        with torch.no_grad():

            y_pred = []
            y_val = []
            attention_list = []
            smiles_list = []
            avg_loss = 0.0
            mean_cm = np.zeros((num_classes, num_classes))
            for idx, batch in enumerate(val_loader):
                smiles, labels = batch[0].to(device), batch[1].to(device)

                out, attention = model(smiles)
                y_val.extend(list(labels.detach().cpu().numpy()))
                y_pred.extend(list(torch.argmax(out, dim=1).detach().cpu().numpy()))
                attention_list.extend(list(attention.detach().cpu().numpy()))
                smiles_list.extend(list(smiles.detach().cpu().numpy()))

                mean_cm = confusion_matrix(y_val, y_pred)

                loss = criterion(out, labels)
                avg_loss += loss.item()
            val_loss.append(avg_loss / len(val_loader))
        
        if scheduling is not None:
            scheduler.step()
    
    # Performance
    scores = defaultdict(float)
    print('\n')
    mcc = matthews_corrcoef(y_val, y_pred)
    scores['mcc'] = mcc
    print(np.round(mcc, 3))
    print(classification_report(y_val, y_pred))
    recall = recall_score(y_val, y_pred, average=None)
    for idx, el in enumerate(recall):
        scores[f'recall_{idx}'] = el
    print('\n')
    
    cm = confusion_matrix(y_val, y_pred)
    print(np.round(cm / np.sum(cm, 1).reshape(-1, 1), 3))
    print('\n')
    
    # Plot losses
    fig, ax = plt.subplots(1, 1, figsize=(15, 7))

    ax.plot(range(len(training_loss)), training_loss, '--o', label='training')
    ax.plot(range(len(val_loss)), val_loss, '--o', label='validation')
    ax.set_xlabel('epoch')
    ax.set_ylabel('loss')
    ax.legend()

    plt.show()

    return smiles_list, attention_list, mapping, mean_cm, scores
