"""
Utility functions for project on SMILES enumeration for 
DILI prediction using RNN or LSTM models.
"""

__author__ = 'Lorenzo Fabbri'

import pandas as pd
import numpy as np

import torch
import torch.nn as nn
from torch.utils.data import TensorDataset

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

from rdkit import Chem

def load_DILIrank(path_file):
    
    data = pd.read_excel(path_file)
    data = data[['Compound Name', 'vDILIConcern', 'SMILES']]
    data.columns = ['compound', 'label', 'smiles']
    
    # Remove compounds w/o SMILES
    data = data[data.smiles != '.']
    
    # Remove ambiguous compounds
    data = data[data.label != 'Ambiguous DILI-concern']
    
    assert data.shape == (721, 3), 'Mismatch DILIrank dataset!'
    
    data.reset_index(inplace=True, drop=True)
    
    return data

def encode_labels(data):
    
    data.reset_index(inplace=True, drop=True)
    
    le = LabelEncoder()
    le.fit(data.label)
    
    data.label = le.transform(data.label)
    
    return data

def to_canonical(data):
    
    data.reset_index(inplace=True, drop=True)
    
    for idx in data.index:
        smi = data.at[idx, 'smiles']
        smi = Chem.MolToSmiles(Chem.MolFromSmiles(smi))
        
        data.at[idx, 'smiles'] = smi
    
    return data

def single_enum(smi):
    
    mol = Chem.MolFromSmiles(smi)
    num_atoms = list(range(mol.GetNumAtoms()))
    np.random.shuffle(num_atoms)
    renumber_atoms = Chem.RenumberAtoms(mol, num_atoms)
    
    new_smi = Chem.MolToSmiles(renumber_atoms, canonical=False, 
                              isomericSmiles=True)
    
    return new_smi

def enum_smiles(data, num_enumerated):
    
    data.reset_index(inplace=True, drop=True)
    
    # Store new SMILES codes and correct label
    res = dict()
    
    pos_res = 0
    for idx in data.index:
        
        # For each SMILES code, enumerate it
        smi = data.at[idx, 'smiles']
        
        for _ in range(num_enumerated):
            new_smi = single_enum(smi)
            
            comp = data.at[idx, 'compound']
            label = data.at[idx, 'label']
            res[pos_res] = {'compound': comp, 'label': label, 'smiles': new_smi}
            pos_res += 1
    
    # Add new SMILES codes to DataFrame
    res_df = pd.DataFrame.from_dict(res, orient='index')
    
    # Concatenate old data and new data
    data = pd.concat([data, res_df], axis=0)
    data.sort_values(by='compound', inplace=True)
    
    return data

def encode_smiles(data, max_len):
    # Integer encoding on SMILES -> associate integer to each character
    
    data.reset_index(inplace=True, drop=True)
    res = []
    
    # Create dictionary for mapping
    alphabet = set(''.join(data.smiles.tolist()))
    mapping = {c: idx for idx, c in enumerate(alphabet, start=1)}
    mapping_reverse = {idx: c for idx, c in enumerate(alphabet, start=1)}
    
    # Encode SMILES codes
    for idx in data.index:
        smi = data.at[idx, 'smiles']
        label = data.at[idx, 'label']
        mat = np.zeros(max_len, dtype=np.int)

        for coln, char in enumerate(smi[:max_len]):
            mat[coln] = mapping[char]
        
        # Add [mat, label] to list
        res.append([mat, label])
    
    assert len(res) == data.shape[0], 'Mismatch number of encoded SMILES!'
    
    return res, mapping

def ohe_smiles(data, max_len):
    
    data.reset_index(inplace=True, drop=True)
    res = []
    
    # Create dictionary for mapping
    alphabet = set(''.join(data.smiles.tolist()))
    mapping = {c: idx for idx, c in enumerate(alphabet, start=1)}
    mapping_reverse = {idx: c for idx, c in enumerate(alphabet, start=1)}
    
    # One-hot encode each character and consider max_len
    for idx in data.index:
        smi = data.at[idx, 'smiles']
        label = data.at[idx, 'label']
        mat = np.zeros((len(mapping)+1, max_len), dtype=np.int)

        for coln, char in enumerate(smi[:max_len]):
            row = mapping[char]
            mat[row, coln] = 1
        
        # Add [mat, label] to list
        res.append([mat, label])
    
    assert len(res) == data.shape[0], 'Mismatch number of OHE matrices!'
    
    return res, mapping

def create_datasets_safe(data, train_size, ohe):
    
    data.reset_index(inplace=True, drop=True)
    
    # Create temporary DataFrame with unique compounds (names)
    data_comp_unique = data.drop_duplicates(subset=['compound'])
    data_comp_unique.reset_index(inplace=True, drop=True)
    
    # Find indices from DataFrame for stratified split based
    # on label
    if train_size is not None:
        train, test, _, _ = train_test_split(data_comp_unique['compound'], 
                                             data_comp_unique['label'], 
                                             stratify=data_comp_unique.label, 
                                             train_size=train_size)
        train_idxs = train.index.tolist()
        test_idxs = test.index.tolist()
        
    elif train_size is None:
        train_idxs = data.index.tolist()
        test_idxs = data.index.tolist()
    
    # Find which compounds where selected for training or validation
    comp_training = data_comp_unique.loc[data_comp_unique.index[train_idxs], 'compound'].tolist()
    comp_val = data_comp_unique.loc[data_comp_unique.index[test_idxs], 'compound'].tolist()
    
    assert set(comp_training) != set(comp_val)
    
    # Find indices in original DataFrame corresponding to respective splits
    train_idxs = data[data['compound'].isin(comp_training)].index
    test_idxs = data[data['compound'].isin(comp_val)].index
    
    assert set(train_idxs.tolist()) != set(test_idxs.tolist())
    
    # Split One-Hot Encoded matrices based on indices
    train_data = [ohe[i] for i in train_idxs]
    val_data = [ohe[i] for i in test_idxs]
    
    ## PyTorch Datasets
    # Training
    train_ds = TensorDataset(torch.stack([torch.tensor(i[0], dtype=torch.long) for i in train_data]), 
                             torch.stack([torch.tensor(i[1], dtype=torch.long) for i in train_data]))
    train_target = np.array([torch.tensor(i[1], dtype=torch.long) for i in train_data])

    # Validation
    val_ds = TensorDataset(torch.stack([torch.tensor(i[0], dtype=torch.long) for i in val_data]), 
                           torch.stack([torch.tensor(i[1], dtype=torch.long) for i in val_data]))
    val_target = np.array([torch.tensor(i[1], dtype=torch.long) for i in val_data])
    
    return train_ds, train_target, val_ds, val_target

def create_datasets(data, train_size, ohe):
    
    data.reset_index(inplace=True, drop=True)
    
    # Find indices from DataFrame for stratified split based
    # on label
    if train_size is not None:
        train, test, _, _ = train_test_split(data['compound'], data['label'], 
                                       stratify=data.label, 
                                       train_size=train_size)
        train_idxs = train.index.tolist()
        test_idxs = test.index.tolist()
        
    elif train_size is None:
        train_idxs = data.index.tolist()
        test_idxs = data.index.tolist()
    
    # Split One-Hot Encoded matrices based on indices
    train_data = [ohe[i] for i in train_idxs]
    val_data = [ohe[i] for i in test_idxs]
    
    ## PyTorch Datasets
    # Training
    train_ds = TensorDataset(torch.stack([torch.tensor(i[0], dtype=torch.float) for i in train_data]), 
                             torch.stack([torch.tensor(i[1], dtype=torch.long) for i in train_data]))
    train_target = np.array([torch.tensor(i[1], dtype=torch.long) for i in train_data])

    # Validation
    val_ds = TensorDataset(torch.stack([torch.tensor(i[0], dtype=torch.float) for i in val_data]), 
                           torch.stack([torch.tensor(i[1], dtype=torch.long) for i in val_data]))
    val_target = np.array([torch.tensor(i[1], dtype=torch.long) for i in val_data])
    
    return train_ds, train_target, val_ds, val_target

class Net(nn.Module):
    
    def __init__(self, input_size, hidden_size, output_size, num_layers):
        super(Net, self).__init__()
        
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        
        self.embedding = nn.Embedding(input_size, hidden_size)
        
        self.rnn = nn.LSTM(hidden_size, hidden_size, num_layers=num_layers, 
                           batch_first=True)
        self.fc = nn.Linear(hidden_size, output_size)
    
    def forward(self, x):
        
        embedded = self.embedding(x)
        
        hidden = torch.zeros(self.num_layers, x.size(0), self.hidden_size)
        output, hidden = self.rnn(embedded, hidden)
        
        out = self.fc(hidden)
        
        return out
