"""
Contains custom estimators for Scikit-learn. E.g., custom scalers.
"""

### Imports ###
import pandas as pd
import numpy as np

import sklearn.base as base
import sklearn.compose
from sklearn.pipeline import Pipeline
import sklearn.preprocessing

### Classes ###
class PreProcess(base.BaseEstimator, base.TransformerMixin, object):
    """
    Applies standardization or normalization techniques. 
    Fitting is performed on training set only.
    
    Methods:
        - Scaling
            * 'StandardScaler' -> zero mean and unit variance
            * 'MinMaxScaler' -> [0, 1] range for the features
        - Normalization
            * 'Normalize' -> scaling individual samples to unit norm.
    """

    def __init__(self, method: str):

        self.method = method
    
    def fit(self, X: pd.DataFrame, y: np.array):

        ## Drop columns
        assert 'label' not in X.columns
        self.original_shape = X.shape
        self.name_colns = X.columns.tolist()

        ## Fit
        accepted_methods = ['StandardScaler', 'MinMaxScaler', 
                           'Normalize']
        assert self.method in accepted_methods

        scaler = eval(f'sklearn.preprocessing.{self.method}()')
        scaler = Pipeline(steps=[('scaler', scaler)])
        identity = Pipeline(steps=[('identity', IdentityTransformer())])

        colns_to_scale = list(X.columns[~X.columns.str.startswith('fps')])
        colns_fps = list(X.columns[X.columns.str.startswith('fps')])

        self.scaler = sklearn.compose.ColumnTransformer(
            remainder='passthrough', 
            transformers = [
                ('scaler', scaler, colns_to_scale), 
                ('identity', identity, colns_fps)
            ], 
            n_jobs=-1
        )
        self.scaler.fit(X, y)
        
        return self
    
    def transform(self, X: pd.DataFrame) -> pd.DataFrame:
        
        if type(X) != pd.core.frame.DataFrame:
            X = pd.DataFrame(X, columns=self.name_colns)

        ## Transform
        assert X.isna().sum().sum() == 0, 'Original dataset contains NaN values.'
        X = self.scaler.transform(X)
        assert np.isnan(X).sum().sum() == 0, 'Transformed dataset contains NaN values.'
        
        ## Concatenate data (processed descriptors + fps)
        # Add column names
        X = pd.DataFrame(X, columns=self.name_colns)
        
        assert X.isna().sum().sum() == 0, 'Concatenated dataset contains NaN values.'
        #assert self.original_shape[0] == X.shape[0], f'Shape of transformed dataset is different {X.shape}.'
        assert 'label' not in X.columns

        return X

class IdentityTransformer(base.BaseEstimator, base.TransformerMixin, object):
    """
    Transformer that just returns the input matrix.
    """

    def __init__(self):
        pass

    def fit(self, X, y):
        return self
    
    def transform(self, X):
        return X

class FeatureSelection(base.BaseEstimator, base.TransformerMixin, object):
    """
    Performs feature selection. E.g., removes features 
    with low or zero variance, features containing 
    NaN values.
    """

    def __init__(self, variance_zero: bool, 
        variance_low: bool, variance_threshold: float, 
        nanNull_columns: bool):

        self.variance_zero = variance_zero
        self.variance_low = variance_low
        self.variance_threshold = variance_threshold
        self.nanNull_columns = nanNull_columns
    
    def fit(self, X: pd.DataFrame, y: np.array = None):

        return self
    
    def transform(self, X: pd.DataFrame) -> pd.DataFrame:

        if self.variance_zero:
            X = X.loc[:, X.var() > 0.0]
        
        if self.variance_low:
            X = X.loc[:, X.var() > self.variance_threshold]

        if self.nanNull_columns:
            X = X.dropna()
        
        return X
