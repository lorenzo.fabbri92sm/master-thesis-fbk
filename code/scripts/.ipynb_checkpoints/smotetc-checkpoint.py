"""
This file contains the class for SMOTE TC.
Reference: https://www.frontiersin.org/articles/10.3389/fchem.2018.00362/full.

Pipeline:
    1. The DataFrame is read, which contains at least one column with the
       name of the compounds and one column with the labels [done]
    2. The SMILES are retrieved for each unique compound
       and molecules are generated with RDKit using a 
       processing pipeline [partially done]
    3. For each minority class:
        3a. Take sample based on amount of over-sampling
            needed for that class [done]
        3b. For each sample, find the k-nearest neighbors
            using the Tanimoto coefficient as similarity
            measure [done]
        3c. Feature vectors are assigned to the new
            synthetic data points as the mode of the
            sample and the k-nearest neighbors [done].

The last step (3c) is different from the one proposed in the reference 
paper since I could not understand their process. Moreover, the actual 
implementation (this one) differs again since if we consider molecular 
descriptors instead of MACCS keys, then we cannot take the mode anymore.

TODO:
    1. Add processing pipeline.
"""

import pandas as pd
import numpy as np
from scipy.spatial.distance import rogerstanimoto
from scipy.stats import mode

from rdkit import Chem
from rdkit.Chem import MACCSkeys
from pubchempy import get_compounds

from collections import Counter
import operator
from random import uniform
from math import ceil

from sklearn.neighbors import NearestNeighbors

class SMOTETC:
    
    def __init__(self, data, k, coln_desc):
        """
        Takes as input the DataFrame, the number k of 
        neighbors and the index of the first column 
        containing the descriptors.
        """
        
        self.data = data
        # Re-name column specific to our dataset
        self.data.rename(columns={'vDili': 'class'}, 
                        inplace=True)
        
        # Number of neighbors
        self.k = k
        # First column with descriptors/features
        self.coln = coln_desc
        
        ### Find minority classes ###
        # Find class frequencies
        counter = Counter(data['class'])
        freqs = dict()
        for label in counter:
            freqs[label] = counter[label] / self.data.shape[0]
        
        # Find majority class
        majority_class = max(freqs.items(), 
                            key=operator.itemgetter(1))[0]
        self.majority_class = majority_class
        self.majority_value = np.max(list(freqs.values()))
        
        # Identify minority classes
        minority_classes = dict()
        for label in freqs:
            #if np.abs(freqs[label]-self.majority_value) >= 0.1:
            if label is not self.majority_class:
                minority_classes[label] = freqs[label]
        self.minority_classes = minority_classes
    
    def get_smiles(self, file):
        """
        Retrieves the SMILES for all unique compounds. Adds 
        column to DataFrame with SMILES.
        """
        
        # Initialize dictionary (name: SMILES)
        compounds = self.data['compound'].tolist()
        unique_compounds = list(set(compounds))
        smiles = {key: None for key in unique_compounds}

        if file:
            path_file_smiles = '../../data/camda19/smiles.csv'
            file_smiles = pd.read_csv(path_file_smiles, 
                                     header=None)
            
            smiles = dict(zip(file_smiles.iloc[:, 0], 
                             file_smiles.iloc[:, 1]))
        
        else:
            # Get molecules from PubChem
            not_found = 0
            for comp in unique_compounds:
                mol = get_compounds(comp, 'name')

                try:
                    mol = mol[0]
                    smiles[comp] = mol.canonical_smiles
                except:
                    not_found += 1

            if not_found != 0:
                print(f'Number of compounds not found: {not_found}.')
        
        # Create temporary DataFrame with SMILES
        df_tmp = pd.DataFrame(list(smiles.items()))
        df_tmp.columns = ['compound', 'smiles']
        
        # Merge DataFrames
        self.data = self.data.merge(df_tmp, on='compound')
    
    def generate_molecules(self):
        """
        Generates RDKit molecules for each unique compound 
        using SMILES.
        """
        
        # Initialize dictionary
        smiles = self.data['smiles'].unique().tolist()
        mols = dict()
        
        # Create RDKit molecules
        for smi in smiles:
            mol = Chem.MolFromSmiles(smi)
            
            name = self.data[self.data['smiles']==smi]['compound']
            
            # Check whether multiple compounds have same SMILES (e.g., iso-)
            if len(self.data[self.data['smiles']==smi]['compound'].unique().tolist()) > 1:
                for c in self.data[self.data['smiles']==smi]['compound'].unique().tolist():
                    mols[c] = mol

            else:
                mols[name.values[0]] = mol
        
        self.mols = mols
    
    def fit(self, classes=None):
        """
        Main function to do over-sampling of minority classes 
        using fingerprints, the Tanimoto coefficient and 
        K-nearest neighbors method.
        """
        
        dfs_all = []
        classes_all = []
        
        # Loop over the minority classes
        for label in self.minority_classes:
            
            # If user provides list of classes, then check
            if classes is not None:
                if label not in classes:
                    continue
                    
            print(f'\tOver-sampling class {label}...')
            
            # Find number of samples needed for this class
            diff = np.abs(self.majority_value - \
                          self.minority_classes[label])
            num_samples = ceil(diff * self.data.shape[0])
            print(f'\t\tConsidering {num_samples} samples.')
            
            # Take sample from DataFrame considering only this class
            df_class = self.data[self.data['class']==label].sample(n=num_samples, 
                                                                  replace=True)
            df_class.reset_index(inplace=True, drop=True)
            
            # For each sample, find k-nearest neighbors
            for sample in df_class.index:
                
                name = df_class.at[sample, 'compound']
                smiles = df_class.at[sample, 'smiles']
                mol = self.mols[name]
                cell_line = df_class.at[sample, 'cell_line']
                
                # Generate synthetic datum
                new_datum = self.find_neighbors(df_class[df_class['compound']==name].sample(n=1), 
                                                df_class[df_class['compound']!=name])
                
                # Save new datum and label
                dfs_all.append(new_datum)
                classes_all.append(label)
        
        # Add new data points to DataFrame
        df_concat = pd.concat(dfs_all)
        df_concat = pd.concat([df_concat, self.data])
        df_concat.drop(['smiles', 'class', 'cell_line', 'compound'], 
                      axis=1, inplace=True)
        
        # Combine all labels
        classes_all.extend(self.data['class'].tolist())
        
        # Return X and y
        return df_concat.values, np.array(classes_all)

    def find_neighbors(self, df_class_sample, df_class_others):
        """
        Uses k-nearest neighbors algorithm to find compounds 
        to be used for SMOTE.
        """
        
        df_class_sample.reset_index(inplace=True, drop=True)
        df_class_others.reset_index(inplace=True, drop=True)
        
        # NumPy array containing fingerprints
        fps = np.zeros((df_class_others.shape[0]+1, 167))
        
        # MACCS keys for sample
        mol = self.mols[df_class_sample.at[0, 'compound']]
        sample_key = MACCSkeys.GenMACCSKeys(mol)
        fps[0] = np.array(sample_key)
        
        # MACCS keys for others
        for idx in df_class_others.index:
            mol = self.mols[df_class_others.at[idx, 'compound']]
            key = MACCSkeys.GenMACCSKeys(mol)
            
            fps[idx+1] = np.array(key)
        
        # Run k-nearest neighbors
        neigh = NearestNeighbors(n_neighbors=self.k, 
                                metric=rogerstanimoto, 
                                n_jobs=-1)
        neigh.fit(fps[1:])
        dist, idxs = neigh.kneighbors(fps[0].reshape(1, -1))
        
        # Generate new datum using standard SMOTE method
        
        vector_sample = df_class_sample.iloc[:, self.coln:-1]
        vector_other = df_class_others.iloc[:, self.coln:-1].sample(n=1)
        vector_other = vector_other * uniform(0, 1)
        
        new_datum = pd.DataFrame(vector_sample.values + vector_other.values)
        new_datum.columns = vector_sample.columns
        
        return new_datum
