"""
Functions for ML models creation, training and evaluation.
"""

import pandas as pd
import pandas_ml
import numpy as np
from collections import defaultdict

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.model_selection import RandomizedSearchCV, cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, BaggingClassifier
from sklearn.metrics import classification_report, accuracy_score, roc_auc_score
from sklearn.metrics import matthews_corrcoef, f1_score, recall_score, log_loss
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler

from imblearn.over_sampling import SMOTE
#import smotetc as stc
import smotetc_par as stc

import warnings
warnings.filterwarnings('ignore')

from functools import partial, reduce
from collections import Counter
import random

from train_val_split_comp import *

cell_lines_unique = ['A375', 'A549', 'HA1E', 'HCC515', 'HEPG2', 
                     'HT29', 'MCF7', 'PC3', 'VCAP', 'SW480', 'ASC', 
                     'NEU', 'NPC', 'PHH', 'SKB', 'BT20', 'HS578T', 
                     'MCF10A', 'MDAMB231', 'SKBR3', 'FIBRNPC', 
                     'NKDBA', 'HEK293T']

# train_size=0.8, num_clusters=50
compounds_train = ['bupropion', 'clofibrate', 'phentermine', 'mexiletine', 'mebendazole', 'albendazole', 'prazosin', 'alfuzosin', 'epirubicin', 'daunorubicin', 'praziquantel', 'naloxone', 'amiodarone', 'donepezil', 'paroxetine', 'raloxifene', 'naltrexone', 'fenoldopam', 'quinidine', 'vecuronium', 'amlodipine', 'felodipine', 'quetiapine', 'perphenazine', 'dipyridamole', 'fluphenazine', 'cetirizine', 'cilostazol', 'anagrelide', 'pimozide', 'risperidone', 'ziprasidone', 'minoxidil', 'chlorambucil', 'fexofenadine', 'vorinostat', 'papaverine', 'febuxostat', 'ketoprofen', 'warfarin', 'bisacodyl', 'isoxsuprine', 'metoprolol', 'acarbose', 'nadolol', 'betaxolol', 'vinblastine', 'rifabutin', 'moxifloxacin', 'vinorelbine', 'dihydroergotamine', 'vincristine', 'clemastine', 'brompheniramine', 'carbinoxamine', 'loperamide', 'escitalopram', 'tadalafil', 'benazepril', 'saquinavir', 'ranolazine', 'moexipril', 'nelfinavir', 'pentoxifylline', 'caffeine', 'famciclovir', 'physostigmine', 'carbamazepine', 'leflunomide', 'primidone', 'tenofovir', 'pindolol', 'primaquine', 'carvedilol', 'metaxalone', 'pazopanib', 'diazoxide', 'metolazone', 'chlorpromazine', 'pergolide', 'chloroquine', 'clozapine', 'meclizine', 'thiothixene', 'clomipramine', 'prochlorperazine', 'thiotepa', 'mesoridazine', 'chlorthalidone', 'bicalutamide', 'clonidine', 'phentolamine', 'isotretinoin', 'estradiol', 'testosterone', 'tretinoin', 'mifepristone', 'exemestane', 'estrone', 'finasteride', 'protriptyline', 'nortriptyline', 'amitriptyline', 'maprotiline', 'cinacalcet', 'desipramine', 'calcitriol', 'digoxin', 'prednisone', 'prednisolone', 'dexamethasone', 'amantadine', 'mitotane', 'diclofenac', 'acitretin', 'diflunisal', 'propofol', 'procainamide', 'lidocaine', 'ropinirole', 'metoclopramide', 'verapamil', 'tramadol', 'doxepin', 'tamoxifen', 'phenoxybenzamine', 'venlafaxine', 'orphenadrine', 'estropipate', 'fulvestrant', 'etodolac', 'ezetimibe', 'fluvastatin', 'oxaprozin', 'scopolamine', 'ketorolac', 'propafenone', 'dobutamine', 'flecainide', 'triamterene', 'guanfacine', 'nilotinib', 'pyrimethamine', 'valsartan', 'irbesartan', 'meloxicam', 'piroxicam', 'methysergide', 'sunitinib', 'felbamate', 'methocarbamol', 'dopamine', 'nimodipine', 'nifedipine', 'tacrolimus', 'erythromycin', 'everolimus', 'sirolimus', 'enalapril', 'thalidomide', 'perindopril', 'dexrazoxane', 'pioglitazone', 'rosiglitazone', 'nizatidine', 'ranitidine', 'sildenafil', 'vardenafil', 'simvastatin', 'lovastatin', 'glipizide', 'bumetanide', 'tolazamide', 'probenecid', 'glimepiride', 'nilutamide', 'dantrolene', 'fluconazole', 'voriconazole', 'procarbazine', 'modafinil', 'omeprazole', 'pantoprazole', 'fludarabine', 'entecavir', 'erlotinib', 'iloperidone', 'gefitinib']
compounds_val = ['alaproclate', 'timolol', 'aliskiren', 'dextromethorphan', 'nalbuphine', 'flavoxate', 'amoxapine', 'aprepitant', 'biperiden', 'fenofibrate', 'bisoprolol', 'bromocriptine', 'reserpine', 'doxylamine', 'diltiazem', 'cabergoline', 'milrinone', 'zolmitriptan', 'celecoxib', 'ticlopidine', 'rizatriptan', 'cimetidine', 'danazol', 'eplerenone', 'duloxetine', 'hydrocortisone', 'sertraline', 'naproxen', 'disopyramide', 'trimethobenzamide', 'toremifene', 'etomidate', 'tolmetin', 'labetalol', 'amiloride', 'losartan', 'rosuvastatin', 'flumazenil', 'midodrine', 'paclitaxel', 'nateglinide', 'pravastatin', 'tolbutamide', 'lapatinib']

# train_size=0.7, num_clusters=30
"""compounds_train = ['alaproclate', 'amantadine', 'mitotane', 'phentermine', 'diclofenac', 'modafinil', 'mexiletine', 'bupropion', 'albendazole', 'tenofovir', 'famciclovir', 'rifabutin', 'vincristine', 'timolol', 'vinblastine', 'alfuzosin', 'saquinavir', 'vinorelbine', 'reserpine', 'aliskiren', 'epirubicin', 'midodrine', 'labetalol', 'vecuronium', 'iloperidone', 'raloxifene', 'quinidine', 'amiodarone', 'donepezil', 'fenoldopam', 'flavoxate', 'gefitinib', 'nifedipine', 'felodipine', 'amlodipine', 'fluphenazine', 'perphenazine', 'chlorambucil', 'cetirizine', 'amoxapine', 'nilotinib', 'clonidine', 'triamterene', 'pyrimethamine', 'milrinone', 'primidone', 'carbamazepine', 'anagrelide', 'fexofenadine', 'biperiden', 'nalbuphine', 'etodolac', 'warfarin', 'propofol', 'bisacodyl', 'diflunisal', 'naproxen', 'fenofibrate', 'papaverine', 'acitretin', 'betaxolol', 'metaxalone', 'acarbose', 'metoprolol', 'bisoprolol', 'propafenone', 'methocarbamol', 'nadolol', 'dopamine', 'escitalopram', 'brompheniramine', 'carbinoxamine', 'loperamide', 'thalidomide', 'ranolazine', 'enalapril', 'perindopril', 'benazepril', 'ezetimibe', 'tadalafil', 'cilostazol', 'caffeine', 'dantrolene', 'pentoxifylline', 'entecavir', 'flumazenil', 'diazoxide', 'chlorthalidone', 'meloxicam', 'pazopanib', 'metolazone', 'rosuvastatin', 'pergolide', 'thiotepa', 'meclizine', 'prochlorperazine', 'chlorpromazine', 'clomipramine', 'desipramine', 'rizatriptan', 'pimozide', 'chloroquine', 'cinacalcet', 'cimetidine', 'maprotiline', 'protriptyline', 'primaquine', 'mifepristone', 'prednisone', 'calcitriol', 'exemestane', 'estradiol', 'digoxin', 'isotretinoin', 'testosterone', 'dexamethasone', 'danazol', 'estrone', 'hydrocortisone', 'lovastatin', 'fulvestrant', 'everolimus', 'paclitaxel', 'tacrolimus', 'sirolimus', 'lidocaine', 'disopyramide', 'procainamide', 'zolmitriptan', 'ropinirole', 'praziquantel', 'phentolamine', 'toremifene', 'orphenadrine', 'tamoxifen', 'venlafaxine', 'amitriptyline', 'tramadol', 'duloxetine', 'doxepin', 'estropipate', 'bumetanide', 'probenecid', 'fluvastatin', 'tolmetin', 'oxaprozin', 'etomidate', 'voriconazole', 'losartan', 'fluconazole', 'cabergoline', 'risperidone', 'metoclopramide', 'methysergide', 'rosiglitazone', 'aprepitant', 'ranitidine', 'lapatinib', 'vardenafil', 'thiothixene', 'glipizide', 'tolbutamide', 'tolazamide', 'pantoprazole']
compounds_val = ['procarbazine', 'nateglinide', 'felbamate', 'vorinostat', 'mebendazole', 'bromocriptine', 'moxifloxacin', 'prazosin', 'dihydroergotamine', 'flecainide', 'diltiazem', 'erlotinib', 'dextromethorphan', 'paroxetine', 'nimodipine', 'dipyridamole', 'quetiapine', 'leflunomide', 'guanfacine', 'amiloride', 'naloxone', 'naltrexone', 'febuxostat', 'clofibrate', 'ketoprofen', 'pindolol', 'dobutamine', 'isoxsuprine', 'carvedilol', 'clemastine', 'doxylamine', 'valsartan', 'moexipril', 'nelfinavir', 'nilutamide', 'fludarabine', 'piroxicam', 'celecoxib', 'clozapine', 'minoxidil', 'mesoridazine', 'ticlopidine', 'nortriptyline', 'sertraline', 'tretinoin', 'prednisolone', 'pravastatin', 'eplerenone', 'simvastatin', 'finasteride', 'erythromycin', 'daunorubicin', 'dexrazoxane', 'ziprasidone', 'physostigmine', 'phenoxybenzamine', 'verapamil', 'trimethobenzamide', 'bicalutamide', 'ketorolac', 'scopolamine', 'irbesartan', 'sunitinib', 'pioglitazone', 'nizatidine', 'sildenafil', 'glimepiride', 'omeprazole']"""

#==================================================#
def ensemble_train(X_all, y_all, model, dict_hyperparams, num_classes, 
                   cv=5, scale=False, sample=False):
    """
    Trains the same ML model class on all the available cell lines 
    independently and then take majority vote for each compound 
    across the cell lines.
    """
    
    X_all.reset_index(drop=True, inplace=True)
    y_all.reset_index(drop=True, inplace=True)
    
    # Create dictionary to store results
    dict_ensemble = dict()
    
    # Train, validation indices
    train_size = 0.8
    print(f'Splitting data with proportions: {train_size}/{np.round(1-train_size, 2)}.\n')
    compounds_train, compounds_val = \
        split_molecules_random(X_all, train_size=train_size)
    idxs_train = X_all[X_all['compound'].isin(compounds_train)].index
    idxs_val = X_all[X_all['compound'].isin(compounds_val)].index
    
    # Iterate over the cell lines present in dataset
    cell_lines = X_all.cell_line.unique().tolist()
    count_cell_lines = 0
    for cell_line in cell_lines:
        print(f'Cell line: {cell_line}...')
        
        # Filter by cell line
        idxs_line = X_all[X_all.cell_line==cell_line].index
        
        # Train, validation splits
        X_train, X_val = X_all.loc[X_all.index[idxs_train].intersection(idxs_line), :], \
                        X_all.loc[X_all.index[idxs_val].intersection(idxs_line), :]
        y_train, y_val = y_all.iloc[idxs_train.intersection(idxs_line)], \
                        y_all.iloc[idxs_val.intersection(idxs_line)]
        
        # Skip cell line if number samples in test set is too small
        if y_val.shape[0] < 20:
            print(f'\tTest set is too small ({y_val.shape[0]}).')
            continue
        else:
            print(f'\tShape of training and validation datasets: {X_train.shape[0], X_val.shape[0]}.\n')
        
        # Check whether enough samples
        if X_train[X_train.cell_line==cell_line].shape[0] < 50:
            print(f'\tNot enough samples ({X_train[X_train.cell_line==cell_line].shape[0]}) for cell line {cell_line}!\n')
            continue
        X_val.drop(['cell_line'], axis=1, inplace=True)
        
        # Check training set contains samples for all classes
        if len(list(y_train.unique()))<num_classes:
            print('\tNot enough classes!')
            continue
        
        # Extract column with compounds for validation
        compounds_val_all = X_val['compound']
        print(f'\tNumber of compounds (unique) for validation: {len(compounds_val_all)} ({len(compounds_val_all.unique())}).')
        X_val.drop('compound', axis=1, inplace=True)
        
        # Drop columns
        X_train.drop('compound', axis=1, inplace=True)
        X_train.drop('cell_line', axis=1, inplace=True)
        
        # Class imbalance
        if sample:
            """dat = pd.concat([pd.Series(y_train), X_train], 
                            axis=1)
            tc = stc.SMOTETC(dat, k=5, coln_desc=3)

            tc.get_smiles(file=True)
            tc.generate_molecules()
            X_train, y_train = tc.fit()"""
            
            sm = SMOTE(random_state=0, n_jobs=-1)
            try:
                X_train, y_train = sm.fit_sample(X_train, y_train)
            except:
                print('\tNot enough samples for SMOTE!')
                continue
        # Count-plot for classes
        if False:
            fig, ax = plt.subplots(1, 1, figsize=(10, 3))
            sns.countplot(y_train, ax=ax)
            plt.show()
        
        # Scaling
        if scale:
            scaler = StandardScaler()
            X_train = pd.DataFrame(scaler.fit_transform(X_train))
            X_val = pd.DataFrame(scaler.transform(X_val))
        
        # Initialize model
        estimator = eval(f'{model}()')
        
        # Model fitting w/ hyper-parameters search
        cv_hyperparams = StratifiedKFold(n_splits=cv)
        rscv = RandomizedSearchCV(estimator, dict_hyperparams, 
                                 random_state=0, n_iter=100, cv=cv_hyperparams, 
                                 verbose=0, n_jobs=-1, 
                                 scoring='recall_weighted')
        best_model = rscv.fit(X_train, y_train)
        
        ##################################################
        print(classification_report(y_val, best_model.predict(X_val)))
        
        cm = confusion_matrix(y_val, best_model.predict(X_val))
        labels = ['no', 'less', 'most'] if num_classes==3 else ['no', 'yes']
        idx = ['t_'+item for item in labels]
        colns = ['p_'+item for item in labels]
        display(pd.DataFrame(cm, index=idx, columns=colns))
        ##################################################
        
        # Model validation
        """predictions = costum_predict(X_train, X_val, 
                                     y_train, y_val, 
                                     best_model)"""
        #predictions = best_model.predict_proba(X_val)[:, 1]
        predictions = best_model.predict(X_val)
        
        # Store true and predicted classes w/ compound names
        tmp_df = pd.concat([compounds_val_all.reset_index(drop=True), 
                            y_val.reset_index(drop=True), 
                            pd.Series(predictions)], 
                           axis=1, ignore_index=True)
        tmp_df.columns = ['compound', 'true_class', 'predicted_class']
        dict_ensemble[cell_line] = tmp_df.copy()
        
        count_cell_lines += 1
        
    return dict_ensemble, count_cell_lines

def costum_predict(X_train, X_val, y_train, y_val, model):
    """
    Performs CV on a set of possible cut-off values, 
    using a custom score as metric.
    """
    
    def costum_score(cutoff):
        def dummy(model, X_train, y_train):
            y_pred = np.argmax(model.predict_proba(X_train) > cutoff, 
                               axis=1)
            
            return recall_score(y_train, y_pred, average='weighted')
        
        return dummy
    
    cutoffs = np.arange(0.1, 0.9, 0.2)
    res = []
    for cutoff in cutoffs:
        
        cv_score = cross_val_score(model, X_train, y_train, 
                                  cv=3, scoring=costum_score(cutoff))
        res.append(np.mean(cv_score))
    
    best_cutoff = np.round(cutoffs[np.argmax(res)], 2)
    predictions = np.argmax(model.predict_proba(X_val) > best_cutoff, 
                            axis=1)
    
    ##################################################
    # Log-loss
    loss = log_loss(y_val, model.predict_proba(X_val)[:, 1])
    print(f'\tLog-loss: {np.round(loss, 2)}.\n')
    ##################################################
    
    return predictions

def split_molecules_random(X, train_size):
    """
    Splits into training and validation sets. Guarantees that each compound is 
    either in training or validation set for each cell line.
    """
    
    # Get all unique compounds
    compounds = X['compound'].unique().tolist()
    
    # Randomly select compounds for training and remaining for validation
    train_size = int(len(compounds) * train_size)
    compounds_train = list(random.sample(compounds, train_size))
    compounds_val = list(set(compounds) - set(compounds_train))
    
    return compounds_train, compounds_val

def eval_ensemble_proba(dict_ensemble, num_classes):
    """
    Returns majority vote for each compound and prints scores. 
    Takes as input probabilities rather then predictions and returns 
    predictions using CV on threshold.
    """
    
    # For each cell line, compute log-likelihood
    ll = {k: [] for k in dict_ensemble.keys()}
    for line, df in dict_ensemble.items():
        ll[line].append(np.sum(np.log(df['predicted_class'].values)))
    
    # Find maximum likelihood among cell lines
    ll_amax = max(ll, key=ll.get)
    ll_max = max(ll.values())[0]
    
    # Compute weights for cell lines
    # Reference: https://stats.stackexchange.com/questions/150995/how-to-combine-weak-classfiers-to-get-a-strong-one
    # Formula is different since I'm using the log-likelihoods
    # and I do not know how to modify this equation
    def numerator(ll_line):
        return np.exp(2*(ll_max / ll_line[0]))

    den = np.sum([numerator(ll_line) for ll_line in ll.values()])
    for line, ll_line in ll.items():
        num = numerator(ll_line)
        res = num / den
        ll[line].append(res)
    
    # Multiply each probability by weight corresponding class
    for line, df in dict_ensemble.items():
        df['predicted_class'] = df['predicted_class'] * ll[line][1]
    
    return eval_ensemble(dict_ensemble, num_classes, 'mean')

def eval_ensemble(dict_ensemble, num_classes, metric='mode'):
    """
    Returns majority vote for each compound and prints scores.
    """
    
    # Iterate over keys and for each DataFrame
    # convert rows corresponding to same compound to
    # columns
    df_all_lines = dict()
    dict_labels = dict()
    for key in dict_ensemble.keys():
        df = dict_ensemble[key]
        
        # Pivot table
        colns = df.iloc[:, [0]+[2]].groupby('compound').cumcount().add(1)
        pt = df.iloc[:, [0]+[2]].pivot_table(index='compound', 
                                            columns=colns).droplevel(0, 
                                                                    axis=1).add_prefix('pred_')
        pt.reset_index(inplace=True)
        
        # Store true classes
        for comp in df['compound'].unique().tolist():
            dict_labels[comp] = df[df['compound']==comp]['true_class'].values[0]
        
        # Store DataFrame in dictionary. Key is cell line
        df_all_lines[key] = pt
    
    # Merge all the resulting DataFrame into a single DataFrame
    merge = partial(pd.merge, on='compound', how='outer')
    merged = reduce(merge, df_all_lines.values())
    
    # Take mode or mean of predictions across cell lines. Take first one
    # if mode
    if metric=='mode':
        met = merged.iloc[:, 1:].mode(axis=1, dropna=True).iloc[:, 0]
    elif metric=='mean':
        met_probs = merged.iloc[:, 1:].mean(axis=1)
        met = (merged.iloc[:, 1:].mean(axis=1) > 0.005).astype(int)
    
    # Combine compound names, true classes and mode of predictions
    res = pd.concat([merged['compound'], met], 
                    axis=1)
    res = res.merge(pd.DataFrame(list(dict_labels.items()), 
                                columns=['compound', 'true_class']), 
                   on='compound')
    res.columns = ['compound', 'met', 'true_class']
    
    # Metrics
    mcc = matthews_corrcoef(res['true_class'], res['met'])
    print(f'MCC: {np.round(mcc, 3)}')
    acc_score = accuracy_score(res['true_class'], res['met'])
    print(f'Accuracy: {np.round(acc_score, 3)}')
    
    if metric=='mean':
        l_loss = log_loss(res['true_class'], met_probs)
        print(f'Log-loss: {np.round(l_loss, 3)}')
    
    if num_classes>2:
        for first in range(num_classes):
            for other in range(first+1, num_classes):
                tmp_df = res[((res.true_class==float(first)) | 
                              (res.true_class==float(other)))]
                roc_auc = roc_auc_score(tmp_df['true_class'], tmp_df['met'])
                print(f'ROC AUC [{first} and {other}]: {np.round(roc_auc, 3)}')
    elif num_classes==2:
        roc_auc = roc_auc_score(res['true_class'], res['met'])
        print(f'ROC AUC: {np.round(roc_auc, 3)}')
    else:
        print('ERROR ROC AUC!')
    
    # Classification report
    print('\n')
    print(classification_report(res['true_class'], res['met']))

def eval_ensemble_binary(list_dicts_ensemble, num_classes):
    """
    Returns majority vote for each compound and prints scores.
    """
    
    # Iterate over keys and for each DataFrame
    # convert rows corresponding to same compound to
    # columns
    df_all_lines = dict()
    dict_labels = dict()
    for binary_idx, binary_class in enumerate(list_dicts_ensemble):
        for key in binary_class.keys():
            df = binary_class[key]

            # Pivot table
            colns = df.iloc[:, [0]+[2]].groupby('compound').cumcount().add(1)
            pt = df.iloc[:, [0]+[2]].pivot_table(index='compound', 
                                                columns=colns).droplevel(0, 
                                                                        axis=1).add_prefix('pred_')
            pt.reset_index(inplace=True)

            # Store true classes
            for comp in df['compound'].unique().tolist():
                dict_labels[comp] = df[df['compound']==comp]['true_class'].values[0]
            
            # Replace '1' with real class
            pt.iloc[:, 1:].replace(1.0, binary_idx+1, inplace=True)

            # Store DataFrame in dictionary. Key is cell line
            df_all_lines[key+f'_{binary_idx+1}'] = pt
    
    # Merge all the resulting DataFrame into a single DataFrame
    merge = partial(pd.merge, on='compound', how='outer')
    merged = reduce(merge, df_all_lines.values())
    
    # Take mode of predictions across cell lines. Take first one
    modes = merged.iloc[:, 1:].mode(axis=1, dropna=True).iloc[:, 0]
    
    # Combine compound names, true classes and mode of predictions
    res = pd.concat([merged['compound'], modes], 
                    axis=1)
    res = res.merge(pd.DataFrame(list(dict_labels.items()), 
                                columns=['compound', 'true_class']), 
                   on='compound')
    res.columns = ['compound', 'mode', 'true_class']
    
    # Metrics
    mcc = matthews_corrcoef(res['true_class'], res['mode'])
    print(f'MCC: {np.round(mcc, 3)}')
    acc_score = accuracy_score(res['true_class'], res['mode'])
    print(f'Accuracy: {np.round(acc_score, 3)}')
    
    # Classification report
    print('\n')
    print(classification_report(res['true_class'], res['mode']))
