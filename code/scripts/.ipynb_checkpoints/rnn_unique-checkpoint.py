import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torch.utils.data import Dataset, DataLoader

import pandas as pd
import numpy as np
import csv
import time

from sklearn.model_selection import train_test_split

import utils_enum as enum

HIDDEN_SIZE = 100
NUM_LAYERS = 2
BATCH_SIZE = 16
NUM_EPOCHS = 5

class DatasetUnique(Dataset):
    
    def __init__(self, path_file):
        
        with open(path_file, 'rt') as f:
            reader = csv.reader(f)
            rows = list(reader)
        
        self.smiles = [row[0] for row in rows]
        self.labels = [row[1] for row in rows]
        self.len = len(self.labels)
        self.labels_list = list(sorted(set(self.labels)))
    
    def __getitem__(self, idx):
        return self.smiles[idx], self.labels[idx]
    
    def __len__(self):
        return self.len
    
    def get_labels(self):
        return self.labels_list
    
    def get_label(self, idx):
        return self.labels_list[idx]
    
    def get_label_id(self, label):
        return self.labels_list.index(label)

## Create dictionary for integer encoding of SMILES codes
data = enum.load_DILIrank('../../data/DILIrank/DILIrank.xls')
data = enum.encode_labels(data)
data = data[['smiles', 'label']]

alphabet = set(''.join(data.smiles.tolist()))
mapping = {c: idx for idx, c in enumerate(alphabet, start=1)}

def split(data, train_size=0.8):
    
    train, val = train_test_split(data, train_size=train_size, 
                                  stratify=data.label)
    
    train.to_csv('../../data/DILIrank/train_rnn.csv', 
                 index=False, header=False)
    val.to_csv('../../data/DILIrank/val_rnn.csv', 
               index=False, header=False)

split(data)

## Datasets and Dataloaders
train_ds = DatasetUnique('../../data/DILIrank/train_rnn.csv')
train_loader = DataLoader(dataset=train_ds, 
                          batch_size=BATCH_SIZE, shuffle=True, 
                          num_workers=2)

val_ds = DatasetUnique('../../data/DILIrank/val_rnn.csv')
val_loader = DataLoader(dataset=val_ds, 
                        batch_size=BATCH_SIZE, shuffle=True, 
                        num_workers=2)

NUM_LABELS = len(train_ds.get_labels())

def pad_smiles(vectorized_smiles, smiles_lengths, labels):
    
    smi_tensor = torch.zeros((len(vectorized_smiles), 
                              smiles_lengths.max())).long()
    
    for idx, (smi, smi_len) in enumerate(zip(vectorized_smiles, 
                                             smiles_lengths)):
        
        smi_tensor[idx, :smi_len] = torch.LongTensor(smi)
    
    # Sort by length
    smiles_lengths, perm_idx = smiles_lengths.sort(0, descending=True)
    smi_tensor = smi_tensor[perm_idx]
    
    # Sort targets accordingly
    target = labels2tensor(labels)
    if len(target):
        target = target[perm_idx]
    
    return smi_tensor, smiles_lengths, target

def make_variables(smiles, labels):
    
    seq_and_len = [str2int(smi) for smi in smiles]
    vectorized_smiles = [el[0] for el in seq_and_len]
    smi_lengths = torch.LongTensor([el[1] for el in seq_and_len])
    
    return pad_smiles(vectorized_smiles, smi_lengths, labels)

def str2int(smi):
    
    arr = [mapping[c] for c in smi]
    
    return arr, len(arr)

def labels2tensor(labels):
    
    labels_idxs = [train_ds.get_label_id(lab) for lab in labels]
    
    return torch.LongTensor(labels_idxs)

class Net(nn.Module):
    
    def __init__(self, input_size, hidden_size, output_size, num_layers):
        super(Net, self).__init__()
        
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        
        self.embedding = nn.Embedding(input_size+1, hidden_size)
        
        self.rnn = nn.LSTM(hidden_size, hidden_size, num_layers=num_layers)
        self.fc = nn.Linear(hidden_size, output_size)
    
    def forward(self, x, smi_lengths):
        
        x = x.t()
        
        embedded = self.embedding(x)
        
        hidden = torch.zeros(self.num_layers, x.size(1), self.hidden_size)
        input_rnn = pack_padded_sequence(embedded, 
                                         smi_lengths.data.cpu().numpy())
        self.rnn.flatten_parameters()
        output, hidden = self.rnn(input_rnn, hidden)
        
        out = self.fc(hidden[-1])
        
        return out
