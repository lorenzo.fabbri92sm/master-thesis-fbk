from rdkit import Chem, DataStructs
from rdkit.Chem import MACCSkeys
from rdkit.Chem import PandasTools, Draw
from rdkit.Chem import rdMolDescriptors as rd_desc
from pubchempy import get_compounds
from functools import reduce
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans

"""
A possibly better way to split compounds for training and validation is to look 
at the similarity between them so that each "type" of compound is present 
in both training and validation in the same proportion.

The idea is the following:
    1. Take as input the name of the compounds and find the unique ones [done]
    2. Retrieve the SMILES for each compound [done]
    3. For each compound generate fingerprints (many available) [done]
    4. Use K-means clustering to cluster the compounds [done]
    5. Start filling the arrays for training and validation (containing the 
        names of the compounds) by taking one compound from each cluster [done]
    6. Return these arrays [done].

    Useful material:
        - http://practicalcheminformatics.blogspot.com/2019/01/k-means-clustering.html
        - https://pubs.acs.org/doi/pdf/10.1021/acs.jcim.8b00663.
"""

def get_smiles(compounds):
    """
    Get the SMILES for each compound in the list compounds.
    Returns a dictionary with keys being the names and as values the SMILES (name: [smilies]).
    """

    # Assert input is a list
    assert type(compounds) == list

    # Initialize dictionary
    unique_compounds = list(set(compounds))
    smiles = {key: [] for key in unique_compounds}

    # Get molecules from PubChem
    not_found = 0
    for comp in unique_compounds:
        mol = get_compounds(comp, 'name')

        try:
            mol = mol[0]
            smiles[comp].append(mol.canonical_smiles)
        except:
            not_found += 1
            smiles[comp].append(None)

    if not_found != 0:
        print(f'Number of compounds not found: {not_found}.')

    return smiles

def get_fingerprints(smiles, fingerprints):
    """
    For each compound in the dictionary smiles (name: smiles) generate the 
    desired fingerprints. Returns the same dictionary but 
    with (name: [smiles, fingerprints]).
    """

    assert type(smiles) == dict

    # Desired number of bits for fingerprints and radius
    n_bits = 2048*2
    rad = 5

    # Create molecules from SMILES
    mols = dict()
    for name, smile in smiles.items():
        mols[name] = Chem.MolFromSmiles(smile[0])

    # Iterate over compounds and generate desired fingerprints
    if fingerprints == 'morgan':
        for comp in smiles.keys():
            mol = mols[comp]

            # Generate fingerprints
            fps = rd_desc.GetMorganFingerprintAsBitVect(mol, rad, nBits=n_bits)

            # Convert RDKit object to NumPy array and store result in dictionary
            fps_np = np.zeros((1,))
            DataStructs.ConvertToNumpyArray(fps, fps_np)
            
            smiles[comp].append(fps_np)
    
    elif fingerprints == 'maccs':
        for comp in smiles.keys():
            mol = mols[comp]

            # Generate fingerprints
            fps = MACCSkeys.GenMACCSKeys(mol)
            
            # Convert RDKit object to NumPy array and store result in dictionary
            fps_np = np.zeros((1,))
            DataStructs.ConvertToNumpyArray(fps, fps_np)
            
            smiles[comp].append(fps_np)

    else:
        print(f'{fingerprints} is not implemented yet!.')
        return

    return smiles

def create_input_data(compound_names, fps_type):
    """
    Create DataFrame using compound names as input.
    """
    
    print(f'Number of unique compounds: {len(compound_names)}.\n')
    
    dict_res = dict()
    
    # Itearte over compounds and get fingerprints
    for name in compound_names:
        smi = get_smiles([name])

        if list(smi.values())[0][0] is not None:
            fps = get_fingerprints(smi, fps_type)
            dict_res[name] = fps[name]
    
    # Create DataFrame
    df = pd.DataFrame.from_dict(dict_res, 
                               orient='index')
    df.columns = ['smiles', 'fps']
    
    return df

def kmeans_clustering(data, num_clusters):
    """
    Runs K-means clustering on the DataFrame containing names, SMILES and fingerprints.
    Returns the DataFrame with a new column indicating the cluster of each compound.
    """
    
    # Create inputs for K-Means clustering
    X = np.array(data.fps.values.tolist())
    
    kmeans = KMeans(n_clusters=num_clusters, 
                   random_state=0, 
                    n_init=100, max_iter=500).fit(X)
    labels = kmeans.labels_
    
    # Add column w/ cluster
    data['cluster'] = list(labels)
    
    return data

def display_clusters(data, dict_dili):
    """
    For each cluster, it shows the belonging compounds.
    """
    
    map_labels = {0: 'no', 
                 1: 'less', 
                 2: 'most', 
                 3: 'ambiguous'}
    
    # Iterate over unique clusters
    for cluster in data['cluster'].unique().tolist():
        print(f'Cluster: {cluster} ({data[data.cluster==cluster].shape[0]}).')
        
        # Create list with RDKit molecules
        mols = [Chem.MolFromSmiles(smi) for smi \
                in data[data.cluster==cluster]['smiles']]
        
        # Find labels for compounds in this cluster
        compounds_cluster = data[data.cluster==cluster].index.unique().tolist()
        labels = []
        for compound in compounds_cluster:
            labels.append(map_labels[dict_dili[compound]])
        
        # Display compounds and corresponding labels (or 
        # count-plot)
        step = 3
        """fig, ax = plt.subplots(1, 1, figsize=(11, 3))
        sns.countplot(labels, ax=ax)
        plt.show()"""
        """for i in range(0, len(compounds_cluster), step):
            #print(f'Labels: {labels[i:i+step]}.')
            display(Draw.MolsToImage(mols[i:i+step], 
                                    subImgSize=(200, 200)))"""
        
        print('='*50)

def train_val_split_comp(data, train_size=0.8):
    """
    Generates the training and validation splits. Returns two list containing 
    the names of the compounds in the training and validation sets, respectively
    """
    
    training_compounds = []
    validation_compounds = []
    data_tmp = data.reset_index()
    
    total_num_compounds = len(data_tmp['index'].unique().tolist())
    total_num_compounds_cp = total_num_compounds
    
    dict_clusters = dict()
    for cluster in data_tmp.cluster.unique():
        df = data_tmp[data_tmp.cluster==cluster]
        
        dict_clusters[cluster] = df['index'].unique().tolist()
    
    # Start filling lists of compounds with compounds from different clusters
    """while total_num_compounds>0:
        
        for cluster in list(dict_clusters):
            
            if len(dict_clusters[cluster])==0:
                del dict_clusters[cluster]
            
            elif len(dict_clusters[cluster])==1:
                training_compounds.append(dict_clusters[cluster][0])
                total_num_compounds -= 1
                del dict_clusters[cluster]
            
            else:
                training_compounds.append(dict_clusters[cluster][0])
                del dict_clusters[cluster][0]
                total_num_compounds -= 1
                
                if len(dict_clusters[cluster])>0:
                    validation_compounds.append(dict_clusters[cluster][0])
                    del dict_clusters[cluster][0]
                    total_num_compounds -= 1"""
    
    df = pd.DataFrame([[k, val] for k, vals in dict_clusters.items() \
                      for val in vals])
    df.columns = ['cluster', 'compound']
    
    for cluster in df['cluster'].unique():
        df_cluster = df[df['cluster']==cluster]
        
        compounds_all = df_cluster['compound'].tolist()
        
        training = df_cluster['compound'].sample(frac=train_size).tolist()
        training_compounds.extend(training)
        
        diff = list(set(compounds_all) - set(training))
        validation_compounds.extend(diff)
    
    assert len(training_compounds)==len(set(training_compounds))
    assert len(validation_compounds)==len(set(validation_compounds))
    assert len(training_compounds)+len(validation_compounds)==total_num_compounds_cp
    assert len(set(training_compounds))+len(set(validation_compounds))==total_num_compounds_cp
    assert len(set(training_compounds)&set(validation_compounds))==0
    
    return training_compounds, validation_compounds
