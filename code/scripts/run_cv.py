"""
Python script to run N independent iterations of 
iterated nested CV.
"""


import utils
import ml_mol
import cv
import plot_confusion_matrix

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import feather
import numpy as np

from collections import defaultdict


### Variables ###

TRAIN_SIZE = 0.8

NESTED = True
CV_STRATEGY = 'StratifiedKFold'
NUM_FOLDS = 5
NUM_ITERATIONS = 10
NUM_EXT_ITERATIONS = 5

PRE_PROCESSING = None

ROUND = 2

# List of dictionaries containing mean and std of scores
# for all models across all external iterations
cv_res_all = []

# Store all values for all scores for all iterations of CV
cv_res_per_iter = defaultdict(list)


### Main function for one iteration of nested CV ###

def run(X: pd.DataFrame, y: pd.Series, models: list, 
        seed: int, mapping: dict, shuffle: bool = False, 
        X_ext: pd.DataFrame = None, y_ext: pd.Series = None, 
        smiles: pd.Series = None, splitter: str = None, sampling: str = 'SMOTE') -> dict:

    # Class initialization
    if NESTED:
        icv = cv.IteratedNestedCrossValidation(seed=seed, train_size=TRAIN_SIZE, 
                                               num_iterations=NUM_ITERATIONS, 
                                               mapping=mapping)
    else:
        icv = cv.IteratedCrossValidation(TRAIN_SIZE, mapping)
    
    
    # Main loop
    if NESTED:
        icv.external_loop(models, smiles=smiles, splitter=splitter, 
                          X=X, y=y, 
                          cv_strategy=CV_STRATEGY, num_folds=NUM_FOLDS, 
                          preprocessing=PRE_PROCESSING, 
                          sampling=sampling, shuffle=shuffle, 
                          X_ext=X_ext, y_ext=y_ext)
    else:
        icv.iterated_cv(models=models, X=X, y=y, 
                        cv_strategy=CV_STRATEGY, num_folds=NUM_FOLDS, 
                        num_iterations=NUM_ITERATIONS, 
                        preprocessing=PRE_PROCESSING, 
                        feature_selection=FEATURE_SELECTION, 
                        sampling=sampling, shuffle=shuffle, 
                        X_ext=X_ext, y_ext=y_ext)
    
    # Report for CV
    model_names = [type(model).__name__ for model in models]
    report_cv = icv.create_report_cv(model_names, NUM_ITERATIONS)
    statistics_cv = icv.compute_statistics_cv(report_cv)

    #print('\n')
    #display(statistics_cv)
    for col in statistics_cv.columns:
        vals = statistics_cv[col].to_list()
        cv_res_per_iter[col].extend([el[0] for el in vals])
    
    cv_res = icv.global_stats(report_cv)
    cv_res_all.append(cv_res)
    #print('-'*160)
    
    # Report for test set
    """
    {'vLess-DILI-Concern': 0, 'vMost-DILI-Concern': 1, 'vNo-DILI-Concern': 2}
    """
    scores_test_set_all_models, conf_mat_test_set_all_models = icv.evaluate_models()
    
    return scores_test_set_all_models, conf_mat_test_set_all_models


### Main function to run nested CV "N" times ###

def bootstrap_ci(arr: np.array) -> list:
    """
    Computes the (1 - alpha) bootstrap confidence interval from 
    the empirical bootstrap distribution of the sample mean. 
    Taken from MLPY. Deafult is 95% confidence interval with 
    1000 iterations.
    """
        
    arr = np.ravel(arr)
    alpha = 0.05
    B = 1000
        
    bmean = np.empty(B, dtype=np.float)
    for b in range(B):
        idx = np.random.random_integers(0, arr.shape[0]-1, arr.shape[0])
        bmean[b] = np.mean(arr[idx])
        
    bmean.sort()
    lower = int(B * (alpha * 0.5))
    upper = int(B * (1 - (alpha * 0.5)))
        
    return [np.round(bmean[lower], ROUND), np.round(bmean[upper], ROUND)]

def run_many(X: pd.DataFrame, y: pd.Series, models: list, 
             mapping: dict, shuffle: bool = False, 
             X_ext: pd.DataFrame = None, y_ext: pd.Series = None, 
             smiles: pd.Series = None, splitter: str = None, sampling: str = 'SMOTE', 
             keys_fig: list = None):
    
    precisions = defaultdict(list)
    recalls = defaultdict(list)
    f1s = defaultdict(list)
    mccs = defaultdict(list)
    baccs = defaultdict(list)
    confusion_matrices = defaultdict(list)
    
    for _iter in range(1, NUM_EXT_ITERATIONS+1):
        #print(f'External iteration: {_iter}/{NUM_EXT_ITERATIONS}.')
        #print('-'*25)
        
        scores_test_set_all_models, conf_mat_test_set_all_models = run(X, y, models, _iter, mapping, shuffle, 
                                                                       X_ext, y_ext, smiles, splitter, sampling)
        
        # Store all scores for averaging and CIs
        model_names = list(scores_test_set_all_models.keys())
        
        for model_name in model_names:
            
            precisions[model_name].append(scores_test_set_all_models[model_name][0])
            recalls[model_name].append(scores_test_set_all_models[model_name][1])
            f1s[model_name].append(scores_test_set_all_models[model_name][2])
            mccs[model_name].append(scores_test_set_all_models[model_name][3])
            baccs[model_name].append(scores_test_set_all_models[model_name][4])
            
            confusion_matrices[model_name].append(conf_mat_test_set_all_models[model_name])
        
        #print('\n')
    
    # Statistics on test sets
    def print_scores_test(dic, score_name, model_name):
        ROUND = 2
        
        print(f'\t{score_name}')
        print(f'\t\tMean: {np.round(np.mean(dic[model_name]), ROUND)}')
        print(f'\t\tStd: {np.round(np.std(dic[model_name]), ROUND)}')
        print(f'\t\tCI: {bootstrap_ci(dic[model_name])}')
    
    print('='*160)
    for model_name in model_names:
        print(f'Model: {model_name}')
        
        # Scores w/ CI
        print_scores_test(baccs, 'Average balanced accuracy', model_name)
        print_scores_test(f1s, 'F1', model_name)
        print_scores_test(mccs, 'MCC', model_name)
        print_scores_test(precisions, 'Precision', model_name)
        print_scores_test(recalls, 'Recall', model_name)
        
        # Confusion matrix
        conf_matrix = np.mean(confusion_matrices[model_name], axis=0)
        keys = '_'.join(keys_fig)
        #path_to_file = f'/Users/lorenzo/Documents/university/UNITN/thesis_local/thesis/images/results/{model_name}_mean_test_{keys}.png'
        path_to_file = None
        plot_confusion_matrix.plot_confusion_matrix(conf_matrix, mapping, path_to_file=path_to_file)
        plt.show()
        
        print('\n')


### Function to run the experiments ###

def run_experiment(data, which_data: str, labels_to_drop: list, 
                   filter_by: list, use_mol_descriptors: bool, 
                   use_top_fingerprints: bool, type_fps: str, 
                   use_cddd: bool, use_gpu: bool, use_file_cddd: bool, 
                   use_additional_descriptors: bool, 
                   use_swissadme: bool, use_stitch_gtex: bool, 
                   use_networks: bool, use_metabolism: bool, 
                   models: list, shuffle: bool, splitter: str, sampling: str):
    """
    Main function to actually run experiments,
    """
    
    # Pre-process dataset
    if which_data == 'DILIrank':
        data = data[data['SMILES'] != '.']
        data = data[['Compound Name', 'vDILIConcern']]
        data.columns = ['compound', 'label']
        data = data[data.label != 'Ambiguous DILI-concern']
    
    # Initialize main class
    mod = ml_mol.DILIMol(data)
    
    # Encode labels
    data, mapping = mod.label_encode(data)
    data.reset_index(inplace=True, drop=True)
    
    # Counts per class
    #sns.countplot(data.label)
    #plt.show()
    
    # Merge and/or drop labels
    if labels_to_drop is not None:
        data = mod.merge_labels(mod.data, labels_to_drop=labels_to_drop)
        data.reset_index(inplace=True, drop=True)
        #print('\n')
    
    # Filter data
    if filter_by is not None:
        data = mod.filter_data(data, by=filter_by)
        data.reset_index(inplace=True, drop=True)
        #print('\n')
    
    # Get SMILES codes
    if which_data == 'DILIrank':
        data = mod.get_smiles(data, use_file=True, 
                              path='../../data/DILIrank/smiles.csv')
    
    # Drop compounds w/ very long SMILES code
    print('\nFiltering dataset by length SMILES.\n')
    data = data[data['smiles'].apply(lambda x: len(x) <= 400)]
    
    # Chemotype curation
    data_curated = mod.chemotype_curation(data)
    data_curated.reset_index(inplace=True, drop=True)
    #print('\n')
    
    # Generate molecular descriptors
    data_desc = mod.generate_descriptors(data_curated, 
                                         use_mol_descriptors=use_mol_descriptors, 
                                         use_top_fingerprints=use_top_fingerprints, 
                                         type_fps=type_fps, 
                                         use_cddd=use_cddd, use_gpu=use_gpu, 
                                         use_file_cddd=use_file_cddd, 
                                         path_in_cddd='../../data/DILIrank/smiles_cddd.csv', 
                                         path_out_cddd='../../data/DILIrank/desc_cddd.csv', 
                                         use_additional_descriptors=use_additional_descriptors, 
                                         use_swissadme=use_swissadme, 
                                         path_swissadme='../../data/DILIrank/swissadme.csv', 
                                         use_stitch_gtex=use_stitch_gtex, 
                                         use_networks=use_networks, use_metabolism=use_metabolism)
    data_desc.drop(['Ipc'], axis=1, inplace=True, errors='ignore')
    data_desc.reset_index(inplace=True, drop=True)

    #return data_desc
    
    # Data
    smiles = data_desc.loc[:, 'smiles']
    X = data_desc.iloc[:, 3:]
    y = data_desc.loc[:, 'label']
    
    # Run
    #print('\n')

    keys_fig = []
    if use_mol_descriptors: keys_fig.append('mol_desc')
    if use_top_fingerprints: keys_fig.append('fps')
    if use_cddd: keys_fig.append('cddd')
    if use_additional_descriptors: keys_fig.append('add_desc')
    if use_swissadme: keys_fig.append('adme')
    if use_stitch_gtex: keys_fig.append('gtex')
    if use_networks: keys_fig.append('nets')
    if use_metabolism: keys_fig.append('met')
    if splitter == 'ScaffoldSplit': keys_fig.append('scaffold')
    if sampling == 'SMOTE': keys_fig.append('smote')
    if shuffle: keys_fig.append('shuffle')

    run_many(X, y, models, mapping, shuffle=shuffle, 
             smiles=smiles, splitter=splitter, sampling=sampling, keys_fig=keys_fig)
    
    return cv_res_all, cv_res_per_iter
