"""
Class to perform iterated cross-validation (CV).
"""

### Constants ###
ROUND = 2

### Imports ###
import pandas as pd
import numpy as np
import itertools
from collections import defaultdict

import custom_split as split
import utils_enum as enum

import warnings
warnings.filterwarnings('ignore', category=DeprecationWarning)

import matplotlib.pyplot as plt
import seaborn as sns

import sklearn
from sklearn import model_selection
from sklearn import pipeline
import sklearn.utils
import sklearn.metrics
from rfpimp import importances, plot_importances
import plot_confusion_matrix

import imblearn
import imblearn.pipeline

import custom_estimators

### Variables ###
hparam_grid = {
    'RandomForestClassifier': {
        'model__n_estimators': [100, 1000], 
        'model__max_depth': [None], 
        'model__min_samples_split': [2], 
        'model__min_samples_leaf': [1], 
        'model__max_features': ['sqrt', 'log2'], 
        'model__n_jobs': [-1], 
        'model__class_weight': ['balanced', 'balanced_subsample']
    }, 
    'GradientBoostingClassifier': {
        'model__n_estimators': [500, 1000], 
        'model__max_features': ['sqrt']
    }, 
    'LGBMClassifier': {
        'model__learning_rate': [0.01], 
        'model__n_estimators': [500], 
        'model__class_weight': ['balanced'], 
        'model__n_jobs': [-1], 
        'model__objective': ['multiclass']
    }, 
    'LogisticRegression': {
        'model__penalty': ['l2'], 
        'model__dual': [False], 
        'model__tol': [1e-5], 
        'model__C': [0.001, 0.01, 0.1, 1.0, 10, 100], 
        'model__class_weight': ['balanced'], 
        'model__solver': ['lbfgs'], 
        'model__max_iter': [10000], 
        'model__multi_class': ['multinomial'], 
        'model__n_jobs': [-1]
    }, 
    'NeuralNetClassifier': {
        'model__lr': [0.01, 0.1, 0.2], 
        'model__module__num_units': [5, 10, 20]
    }, 
    'LinearSVC': {
        'model__penalty': ['l2'], 
        'model__loss': ['squared_hinge'], 
        'model__tol': [1e-7], 
        'model__C': [0.00001, 0.0001, 0.001, 0.01, 0.1], 
        'model__class_weight': ['balanced'], 
        'model__random_state': [0], 
        'model__max_iter': [100000]
    }, 
    'SVC': {
        'model__C': [0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1.0, 10.0], 
        'model__gamma': ['auto', 'scale'], 
        'model__tol': [1e-7], 
        'model__class_weight': ['balanced'], 
        'model__random_state': [0]
    }, 
    'BalancedRandomForestClassifier': {
        'model__n_estimators': [1000], 
        'model__sampling_strategy': ['not majority'], 
        'model__n_jobs': [-1], 
        'model__class_weight': ['balanced_subsample']
    }, 
    'SuperLearner': {}
}

### Classes ###
class IteratedCrossValidation:
    
    def __init__(self, train_size: float = 0.8, mapping: dict = None):

        self.train_size = train_size
        self.mapping = mapping
        
        # Dictionary to store DataFrames with all results
        self.dict_res = dict()
        # List to store fitted models
        self.best_models = []
        # Dictionary storing y_true and y_pred for all models
        self.preds = {}
        # Dictionary to store results of all methods for all iterations
        self.results_all = dict()

        self.statistics = ['mean', 'std']
        self.metrics = ['balanced_accuracy', 'f1_weighted', 
                        'precision_weighted', 'recall_weighted', 
                        'mcc']
        self.column_names_df_results = [f'{stat}_{metric}' for \
            stat, metric in itertools.product(self.statistics, self.metrics)]
    
    def iterated_cv(self, models: list, 
                    X: pd.DataFrame, 
                    y: pd.Series, 
                    cv_strategy, num_folds: int, 
                    num_iterations: int = 10, 
                    preprocessing: str = None, 
                    feature_selection: dict = None, 
                    sampling: str = None, shuffle: bool = False, 
                    X_ext: pd.DataFrame = None, y_ext: pd.DataFrame = None):
        """
        Performs main loop: iterates over the number of 
        CV iterations (e.g., the 10 in 10x5 CV).
        """

        # Initialize dictionary of results
        self.results_all = dict()
        self.results_all = {type(model).__name__: [] for model in models}
        self.num_iterations = num_iterations
        
        # Iterate over number of iterations for all models
        for iteration in range(num_iterations):
            print(f'Iteration {iteration+1}/{num_iterations}...')

            # Split data into train/validation and test. Convert to DataFrames
            if self.train_size is not None:
                X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, 
                                                                                            train_size=self.train_size, 
                                                                                            random_state=iteration)
            else:
                X_train, y_train = X, y
                X_test, y_test = X_ext, y_ext
                
            X_train.reset_index(inplace=True, drop=True)
            X_test.reset_index(inplace=True, drop=True)
            y_train = pd.Series(y_train)
            y_test = pd.Series(y_test)
            
            if shuffle:
                y_train = y_train.sample(frac=1)
                y_train.reset_index(inplace=True, drop=True)
                
                y_test = y_test.sample(frac=1)
                y_test.reset_index(inplace=True, drop=True)
            
            self.perform_iteration_models(iteration, models, 
                                        X_train, y_train, 
                                        X_test, y_test, 
                                        cv_strategy, num_folds, 
                                        preprocessing, feature_selection, 
                                        sampling)
    
    def perform_iteration_models(self, iteration: int, models: list, 
                                X_train: pd.DataFrame, y_train: pd.Series, 
                                X_test: pd.DataFrame, y_test: pd.Series, 
                                cv_strategy, num_folds: int, 
                                preprocessing: str = None, 
                                feature_selection: dict = None, 
                                sampling: str = None, enumeration: str = None):
        """
        Performs one iteration of CV considering all models.
        """

        for model in models:

            # CV
            cross_validator = eval(f'model_selection.{cv_strategy}(n_splits={num_folds}, shuffle=True, random_state={iteration})')

            # Sampling
            sampler = eval(f'imblearn.over_sampling.{sampling}(random_state={iteration}, n_jobs=-1)') if sampling else None
            
            # SMILES enumeration for data augmentation
            if enumeration is not None:
                
                # Create DataFrame with both descriptors and labels
                X_train.insert(0, 'label', y_train)
                
                # Enumeration step and drop duplicates by SMILES
                data_enumerated = enum.enum_smiles(X_train, enumeration)
                
                # Create X_train and y_train
            
            # Pre-processing
            preprocess = custom_estimators.PreProcess(preprocessing) if preprocessing else None

            # Feature selection
            #feature_selector = custom_estimators.FeatureSelection(**feature_selection) if feature_selection else None
            
            # Ability to handle PyTorch models w/ Skorch. Need to convert DataFrames
            # to NumPy array
            def to_array(X: pd.DataFrame, dtype=np.float32):
                return np.array(X, dtype=dtype)
            
            model_name = type(model).__name__
            arr = sklearn.preprocessing.FunctionTransformer(to_array, validate=False) if model_name == 'NeuralNetClassifier' else None

            # Create pipeline
            pipe = imblearn.pipeline.Pipeline(steps=[('preprocessing', preprocess), 
                                                    ('sampling', sampler), 
                                                    ('toarray', arr), 
                                                    ('model', model)], verbose=True)
            
            # Search and store best models
            bacc = sklearn.metrics.make_scorer(sklearn.metrics.balanced_accuracy_score)
            f1 = sklearn.metrics.make_scorer(sklearn.metrics.f1_score, average='weighted')
            prec = sklearn.metrics.make_scorer(sklearn.metrics.precision_score, average='weighted')
            rec = sklearn.metrics.make_scorer(sklearn.metrics.recall_score, average='weighted')
            mcc = sklearn.metrics.make_scorer(sklearn.metrics.matthews_corrcoef)
            scoring = {'balanced_accuracy': bacc, 'f1_weighted': f1, 
                        'precision_weighted': prec, 'recall_weighted': rec, 
                        'mcc': mcc}
            search = model_selection.GridSearchCV(pipe, hparam_grid[model_name], 
                                                cv=cross_validator, 
                                                n_jobs=-1, scoring=scoring, refit='balanced_accuracy')
            search.fit(X_train, y_train)
            self.best_models.append(search.best_estimator_)

            # Store predictions
            def predict(X: pd.DataFrame, search, steps: list):
                if model_name == 'NeuralNetClassifier':
                    X = to_array(X)
                    
                for step in steps:
                    assert list(search.estimator.named_steps.keys())[step] != 'sampling'
                    
                    X = search.best_estimator_[step].transform(X)
                
                if model_name == 'NeuralNetClassifier':
                    X = to_array(X)
                    
                return pd.Series(search.best_estimator_[-1].predict(X))
            
            y_pred = predict(X_test, search, [0])
            self.preds[f'{model_name}_{iteration}'] = (y_test, y_pred)

            # Store results (mean and std)
            best_idx = search.best_index_
            res_model = []
            for stat, metric in itertools.product(self.statistics, self.metrics):
                val = search.cv_results_[f'{stat}_test_{metric}'][best_idx]
                res_model.append(val)
            self.results_all[model_name].append(res_model)
    
    def evaluate_models(self):
        """
        Computes scores for evaluating a fitted model on the test set.
        """

        keys = self.preds.keys()
        name_models = set([name.split('_')[0] for name in keys])
        unique_classes = list(np.unique(list(self.preds.values())))
        unique_classes = list(map(str, unique_classes))
        number_unique_classes = len(unique_classes)

        # Iterate over the models and then over the CV iterations
        for name in name_models:
            print(f'Model: {name}')

            # Store scores for specific model
            scores = []
            average = None
            conf_matrices = np.zeros((number_unique_classes, number_unique_classes))
            hist = {label: [] for label in unique_classes}  # Store recall scores for all classes
            vals_for_conf_intervals = defaultdict(list)  # CIs
            for key in keys:

                if key.startswith(name):
                    y_true = self.preds[key][0]
                    y_pred = self.preds[key][1]
                    args = {'y_true': y_true, 'y_pred': y_pred, 
                            'average': average}
                    precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(**args)
                    mcc = np.repeat(np.array(sklearn.metrics.matthews_corrcoef(y_true, y_pred)), number_unique_classes)
                    scores.append([precision, recall, f_score, mcc, support])

                    conf_matrices += sklearn.metrics.confusion_matrix(self.preds[key][0], self.preds[key][1])

                    which_to_plot = 'recall'
                    for label, s in enumerate(recall):
                        hist[str(label)].append(s)
                    
                    # Append values for computing CIs
                    vals_for_conf_intervals['precision'].append(precision)
                    vals_for_conf_intervals['recall'].append(recall)
                    vals_for_conf_intervals['f_score'].append(f_score)
                    vals_for_conf_intervals['mcc'].append(mcc)
            
            # Compute CIs for all metrics
            precision_ci_all_classes = [self.confidence_interval_bootstrapping(vals_for_conf_intervals['precision'][c]) for c in range(number_unique_classes)]
            recall_ci_all_classes = [self.confidence_interval_bootstrapping(vals_for_conf_intervals['recall'][c]) for c in range(number_unique_classes)]
            f_score_ci_all_classes = [self.confidence_interval_bootstrapping(vals_for_conf_intervals['f_score'][c]) for c in range(number_unique_classes)]
            mcc_ci_all_classes = [self.confidence_interval_bootstrapping(vals_for_conf_intervals['mcc'][c]) for c in range(number_unique_classes)]
            all_ci_all_classes = [f_score_ci_all_classes, mcc_ci_all_classes, precision_ci_all_classes, recall_ci_all_classes]
            
            lower = []
            upper = []
            for score_ci in all_ci_all_classes:
                lower.append([el[0] for el in score_ci])
                upper.append([el[1] for el in score_ci])
                
            lower = np.array(lower).T
            lower = np.hstack((lower, -9 * np.ones((number_unique_classes, 1))))
            upper = np.array(upper).T
            upper = np.hstack((upper, -9 * np.ones((number_unique_classes, 1))))
            
            scores = [np.concatenate(score) for score in scores]
            
            # Average scores for specific method and per class
            avg = np.average(np.array(scores), axis=0)
            std = np.std(np.array(scores), axis=0)
            df = pd.DataFrame(np.stack([avg, std]).transpose()).T

            score_names = ['precision', 'recall', 'f_score', 'mcc']
            mapping_inv = {str(val): key for key, val in self.mapping.items()}
            unique_classes = [mapping_inv[c] for c in unique_classes]
            column_names = ['_'.join(el) for el in list(itertools.product(score_names, unique_classes))]
            column_names.extend([f'support_{i}' for i in unique_classes])
            df.columns = pd.MultiIndex.from_product([score_names+['support'], unique_classes], 
                                                    names=['score', 'class'])
            df = df.swaplevel(0, 1, 1).sort_index(axis=1)
            df.index = self.statistics
            
            # CIs
            df.loc['lower'] = list(lower.flatten().T)
            df.loc['upper'] = list(upper.flatten().T)
            
            #display(df.round(ROUND))

            # Mean confusion matrices (counts and probabilities)
            conf_matrices = conf_matrices / self.num_iterations
            plot_confusion_matrix.plot_confusion_matrix(conf_matrices, 
                                                        list(self.mapping.keys()))
            plt.show()

            # Distribution of scores for different classes
            self.plot_distribution(hist, which_to_plot)
            plt.show()

            print('-'*100, '\n')
        
    def confidence_interval_bootstrapping(self, data, confidence=0.95, 
                                          iterations=1000):
        
        stats = []
        n_size = len(data)
        
        for _ in range(iterations):
            sample = sklearn.utils.resample(data, n_samples=n_size)
            stats.append(np.mean(sample))
        
        ordered_stats = sorted(stats)
        lower = np.percentile(ordered_stats, ((1 - confidence) / 2) * 100)
        upper = np.percentile(ordered_stats, (confidence + ((1 - confidence) / 2)) * 100)
        
        return (lower, upper)
    
    def plot_distribution(self, vals: dict, score: str):
        """
        Plots distribution of score for different classes.
        """

        fig, ax = plt.subplots(1, 1, figsize=(10, 5))
        mapping = {v: k for k, v in self.mapping.items()}

        # Iterate over labels
        for label in vals.keys():
            sns.distplot(vals[label], ax=ax, 
                        rug=False, hist=False, 
                        label=mapping[int(label)])
        
        plt.legend()
        ax.set_title(f'Distribution {score} for test set.')
        ax.set_xlabel(f'{score}')
        
        return ax

    def create_report_cv(self, model_names: list) -> pd.DataFrame:
        """
        Creates Pandas DataFrame containing the results for all the 
        models and all the iterations.
        """

        scores = ['_'.join(list(score)) for score in list(itertools.product(self.statistics, self.metrics))]
        multi_idx = pd.MultiIndex.from_product([model_names, scores], names=['models', 'scores'])
        df_results = pd.DataFrame(index=multi_idx)

        # Iterate over models and then over iterations. Create DataFrame
        dict_res_iterations = {iteration: [] for iteration in range(self.num_iterations)}
        for idx_model, list_res_model in enumerate(self.results_all.values()):
            for idx_iter, list_res_iter in enumerate(list_res_model):
                dict_res_iterations[idx_iter].extend(list_res_iter)
        
        for iteration in dict_res_iterations.keys():
            df_results[f'iter_{iteration+1}'] = dict_res_iterations[iteration]
        
        # Insert columns with mean scores across iterations
        df_results.insert(0, 'mean', list(df_results.loc[:, 'iter_1':].mean(axis=1)))
        df_results.insert(1, 'std', list(df_results.loc[:, 'iter_1':].std(axis=1)))
        df_results.insert(2, 'max', list(df_results.loc[:, 'iter_1':].max(axis=1)))
        df_results.insert(3, 'min', list(df_results.loc[:, 'iter_1':].min(axis=1)))

        return df_results.round(ROUND)


class IteratedNestedCrossValidation:
    
    def __init__(self, seed: int, train_size: float = 0.8, 
                 num_iterations: int = 10, 
                 mapping: dict = None):

        self.seed = seed
        self.train_size = train_size
        self.num_iterations = num_iterations
        self.mapping = mapping
        
        self.splitters = ['ScaffoldSplit']
        
        self.best_estimators = {}  # For each model, store search results
        self.preds = {}  # Predictions on test set for all models

        self.statistics = ['mean', 'std']
        self.metrics = ['balanced_accuracy', 'f1_weighted', 
                        'precision_weighted', 'recall_weighted', 
                        'mcc']
        self.column_names_df_results = [f'{stat}_{metric}' for \
            stat, metric in itertools.product(self.statistics, self.metrics)]
        
        self.all_iters_all_scores_all_folds = defaultdict(lambda: defaultdict())
    
    def external_loop(self, models: list, smiles: pd.Series, splitter: str, 
                      X: pd.DataFrame, y: pd.Series, 
                      cv_strategy, num_folds: int, 
                      preprocessing: str = None, 
                      sampling: str = None, shuffle: bool = False, 
                      X_ext: pd.DataFrame = None, y_ext: pd.DataFrame = None):
        """
        Performs main loop: splits dataset into training/validation 
        and test sets. The training set then goes into the nested 
        loop to performs several iterations of CV.
        """
        
        X.reset_index(inplace=True, drop=True)
        y.reset_index(inplace=True, drop=True)
        smiles.reset_index(inplace=True, drop=True)
        
        if self.train_size is not None:
            # Split original dataset
            
            self.custom_splitting = False
            splitter_name = None
            if splitter in self.splitters:
                self.custom_splitting = True
                
                # Shuffle smiles, X and y the same way
                shuffled_idxs = np.random.RandomState(seed=self.seed).permutation(X.index)
                X = X.reindex(shuffled_idxs)
                y = y.reindex(shuffled_idxs)
                smiles = smiles.reindex(shuffled_idxs)
                X.reset_index(inplace=True, drop=True)
                y.reset_index(inplace=True, drop=True)
                smiles.reset_index(inplace=True, drop=True)
                
                # Split
                splitter_name = splitter
                splitter = eval(f'split.{splitter}(train_size={self.train_size}, seed={self.seed})')
                train_idxs, val_idxs = splitter.split(smiles, X, y, stratify=None)
                
                X_train, X_test = X.iloc[train_idxs, :], X.iloc[val_idxs, :]
                y_train, y_test = y[train_idxs], y[val_idxs]
                smiles_train = smiles[train_idxs]
                
            else:
                X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, stratify=y, 
                                                                                            train_size=self.train_size, 
                                                                                            random_state=self.seed, shuffle=True)
        else:
            # Use external dataset for testing. Does not currently
            # support custom splitters
            X_train, y_train = X, y
            X_test, y_test = X_ext, y_ext
        
        X_train.reset_index(inplace=True, drop=True)
        X_test.reset_index(inplace=True, drop=True)
        y_train = pd.Series(y_train)
        y_train.reset_index(inplace=True, drop=True)
        y_test = pd.Series(y_test)
        y_test.reset_index(inplace=True, drop=True)
        if splitter_name is not None:
            smiles_train.reset_index(inplace=True, drop=True)
        else:
            smiles_train = None
        
        if shuffle:
            # Randomly shuffle the labels for both sets
            y_train = y_train.sample(frac=1)
            y_train.reset_index(inplace=True, drop=True)
            y_test = y_test.sample(frac=1)
            y_test.reset_index(inplace=True, drop=True)
        
        # Perform iterations of nested CV
        for _iter in range(1, self.num_iterations+1):
            #print(f'Iteration: {_iter}/{self.num_iterations}...')
            
            # Nested CV
            self.nested_cv(_iter, models, 
                           X_train, y_train, 
                           cv_strategy, num_folds, 
                           preprocessing, sampling, 
                           splitter_name, smiles_train)
            
        # Evaluation on test set for all models
        def predict(X: pd.DataFrame, search, steps: list):
            for step in steps:
                if search.best_estimator_[step] is not None:
                    assert list(search.estimator.named_steps.keys())[step] != 'sampling'

                    X = search.best_estimator_[step].transform(X)
            
            return pd.Series(search.best_estimator_[-1].predict(X))
        
        for model_name in self.best_estimators.keys():
            search = self.best_estimators[model_name][0]
            y_pred = predict(X_test, search, [0])
            self.preds[model_name] = [y_test, y_pred]

            """# FI
            imp = importances(search.best_estimator_[-1], X_test, y_test)
            viz = plot_importances(imp)
            viz.view()"""
    
    def nested_cv(self, _iter: int, models: list, 
                  X_train: pd.DataFrame, y_train: pd.Series, 
                  cv_strategy, num_folds: int, 
                  preprocessing: str = None, sampling: str = None, 
                  splitter_name: str = None, smiles_train: pd.Series = None):
        """
        Performs one iteration of nested CV using the 
        training/valifation set.
        """
        
        X_train.reset_index(inplace=True, drop=True)
        y_train.reset_index(inplace=True, drop=True)
        if smiles_train is not None:
            smiles_train.reset_index(inplace=True, drop=True)
        
        for model in models:
            model_name = type(model).__name__
            #print(f'\tModel: {model_name}...')
            
            # Pre-processing
            preprocess = custom_estimators.PreProcess(preprocessing) if preprocessing else None

            # Sampling
            sampler = eval(f'imblearn.over_sampling.{sampling}(random_state={_iter}, n_jobs=-1)') if sampling else None
            
            # Create pipeline
            pipe = imblearn.pipeline.Pipeline(steps=[('preprocessing', preprocess), 
                                                    ('sampling', sampler), 
                                                    ('model', model)], verbose=True)
            
            # Scores
            bacc = sklearn.metrics.make_scorer(sklearn.metrics.balanced_accuracy_score)
            f1 = sklearn.metrics.make_scorer(sklearn.metrics.f1_score, average='weighted')
            prec = sklearn.metrics.make_scorer(sklearn.metrics.precision_score, average='weighted')
            rec = sklearn.metrics.make_scorer(sklearn.metrics.recall_score, average='weighted')
            mcc = sklearn.metrics.make_scorer(sklearn.metrics.matthews_corrcoef)
            
            scoring = {'balanced_accuracy': bacc, 'f1_weighted': f1, 
                       'precision_weighted': prec, 'recall_weighted': rec, 
                       'mcc': mcc}
            
            # Search
            if self.custom_splitting == True:
                
                # Store indices for all folds
                folds_idxs = []
                
                for fold in range(num_folds):
                
                    # Shuffle smiles, X and y the same way
                    shuffled_idxs = np.random.RandomState(seed=_iter).permutation(X_train.index)
                    X_train = X_train.reindex(shuffled_idxs)
                    y_train = y_train.reindex(shuffled_idxs)
                    smiles_train = smiles_train.reindex(shuffled_idxs)
                    X_train.reset_index(inplace=True, drop=True)
                    y_train.reset_index(inplace=True, drop=True)
                    smiles_train.reset_index(inplace=True, drop=True)

                    # Split
                    splitter = eval(f'split.{splitter_name}(train_size={self.train_size}, seed={_iter})')
                    train_idxs, val_idxs = splitter.split(smiles_train, X_train, y_train, stratify=None)
                    
                    folds_idxs.append((train_idxs, val_idxs))
                
                cross_validator = iter([(idxs[0], idxs[1]) for idxs in folds_idxs])
                
            else:
                cross_validator = eval(f'model_selection.{cv_strategy}(n_splits={num_folds}, shuffle=True, random_state={_iter})')
            
            search = model_selection.GridSearchCV(pipe, hparam_grid[model_name], 
                                                  cv=cross_validator, 
                                                  n_jobs=-1, scoring=scoring, refit='balanced_accuracy')
            search.fit(X_train.values, y_train.values)  # Must use .values if using custom cross-validator
            #search.fit(X_train, y_train)
            
            # Store best estimator for each model
            best_estimator_idx = search.best_index_
            best_score = search.cv_results_['mean_test_balanced_accuracy'][best_estimator_idx]  # Default accuracy
            
            if model_name not in self.best_estimators.keys():
                self.best_estimators[model_name] = [search, best_score]
            else:
                # If not first iteration, check if current model peforms better
                if self.best_estimators[model_name][1] < best_score:
                    self.best_estimators[model_name] = [search, best_score]
            
            # Store results
            all_scores_all_folds = self.get_results(search, model_name, num_folds, list(scoring.keys()))
            self.all_iters_all_scores_all_folds[_iter][model_name] = all_scores_all_folds
    
    def get_results(self, search, model_name: str, 
                    num_folds: int, scores: list) -> dict:
        """
        Returns all computed scores for all folds.
        """
        
        cv_res = search.cv_results_
        best_idx = search.best_index_
        
        res_all_scores_all_folds = defaultdict(list)
        
        for score in scores:
            _scores = []
            
            for _iter in range(num_folds):
                
                which = f'split{_iter}_test_{score}'
                res_score_best_model = cv_res[which][best_idx]
                _scores.append(res_score_best_model)
            
            res_all_scores_all_folds[score] = _scores
        
        return res_all_scores_all_folds

    def create_report_cv(self, model_names: list, num_iterations: int) -> pd.DataFrame:
        """
        Creates Pandas DataFrame containing the results for all the 
        models and all the iterations of nested CV.
        """
        
        all_dfs = [pd.DataFrame.from_dict(self.all_iters_all_scores_all_folds[i], orient='index') for i in range(1, num_iterations+1)]
        df_all_models_all_iters = pd.concat(all_dfs).reset_index()
        
        n = list(range(1, num_iterations+1))
        iters = list(itertools.chain.from_iterable(zip(*itertools.repeat(n, len(model_names)))))
        
        df_all_models_all_iters['iter'] = iters
        
        df_all_models_all_iters = pd.pivot_table(df_all_models_all_iters, index=['index', 'iter'], aggfunc='first')
        
        return df_all_models_all_iters
    
    def compute_statistics_cv(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Generates DataFrame with statistics (mean, std, CIs) for all scores 
        for all models. Mean and std are computed by iteration.
        """
        
        # Statistics (mean and std per iteration)
        df_stats = df.copy()
        
        for idx_row in df_stats.index:
            for idx_coln, _ in enumerate(df_stats.loc[idx_row]):
                mean = np.round(np.mean(df_stats.loc[idx_row][idx_coln]), ROUND)
                std = np.round(np.std(df_stats.loc[idx_row][idx_coln]), ROUND)
            
                df_stats.loc[idx_row][idx_coln] = [mean, std]
        
        return df_stats

    def global_stats(self, df: pd.DataFrame) -> dict:
        """
        Computes global statistics (per model): mean, 
        standard deviation and CIs for all metrics.
        """

        res_dict = defaultdict(dict)
        
        # Aggregate for each model results from all iterations
        models = list(df.index.levels[0])
        
        for model in models:
            #print(f'Model: {model}')
            df_model = df.loc[model]
            
            # Iterate over metrics
            for coln in df_model.columns:
                #print(f'\tMetric: {coln}')
                
                all_vals = np.array(df_model[coln].tolist()).flatten()
                mean = np.round(np.mean(all_vals), ROUND)
                std = np.round(np.std(all_vals), ROUND)
                res_dict[model][coln] = (mean, std)
                #print(f'\t\tMean: {mean} - Std: {std}')
                
                # CIs
                ci = self.bootstrap_ci(all_vals)
                #print(f'\t\tCI: {ci}')
            
            #print('\n')
        
        return res_dict
    
    def evaluate_models(self):
        """
        Computes scores for evaluating a fitted model on the test set.
        """
        
        scores_test_set_all_models = {}
        conf_mat_test_set_all_models = {}
        
        # Iterate over models
        for model_name in self.preds.keys():
            #print(f'Model: {model_name}')
            
            y_true = self.preds[model_name][0]
            y_pred = self.preds[model_name][1]
            
            # Compute scores
            average = None
            args = {'y_true': y_true, 'y_pred': y_pred, 
                    'average': average}
            precision, recall, f_score, support = sklearn.metrics.precision_recall_fscore_support(**args)
            mcc = sklearn.metrics.matthews_corrcoef(y_true, y_pred)
            balanced_acc = sklearn.metrics.balanced_accuracy_score(y_true, y_pred)
            scores_test_set_all_models[model_name] = [precision, recall, f_score, mcc, balanced_acc]
            #print(f'\tPRE: {np.round(precision, ROUND)} \t REC: {np.round(recall, ROUND)} \t F1: {np.round(f_score, ROUND)} \t MCC: {np.round(mcc, ROUND)}.\n')
            
            conf_matrix = sklearn.metrics.confusion_matrix(y_true, y_pred)
            conf_mat_test_set_all_models[model_name] = conf_matrix
            #plot_confusion_matrix.plot_confusion_matrix(conf_matrix, self.mapping, None)
            #plt.show()
            
            #print('\n')
        
        return scores_test_set_all_models, conf_mat_test_set_all_models
    
    def bootstrap_ci(self, arr: np.array) -> list:
        """
        Computes the (1 - alpha) bootstrap confidence interval from 
        the empirical bootstrap distribution of the sample mean. 
        Taken from MLPY. Deafult is 95% confidence interval with 
        1000 iterations.
        """
        
        arr = np.ravel(arr)
        alpha = 0.05
        B = 1000
        
        bmean = np.empty(B, dtype=np.float)
        for b in range(B):
            idx = np.random.random_integers(0, arr.shape[0]-1, arr.shape[0])
            bmean[b] = np.mean(arr[idx])
        
        bmean.sort()
        lower = int(B * (alpha * 0.5))
        upper = int(B * (1 - (alpha * 0.5)))
        
        return [np.round(bmean[lower], ROUND), np.round(bmean[upper], ROUND)]
