"""
Custom splitters to generate training and validation sets.
    
It can be used to replace both `train_test_split` and any 
cross-validator (e.g., the `cv` in `GridSearchCV`).

Most of the code is based on the DeepChem's splitters.
"""

from rdkit import Chem
from rdkit.Chem.Scaffolds import MurckoScaffold

__author__ = 'Lorenzo Fabbri'


class ScaffoldGenerator(object):
        
    def __init__(self, include_chirality=False):
        self.include_chirality = include_chirality
        
    def get_scaffold(self, mol):
            
        return MurckoScaffold.MurckoScaffoldSmiles(mol=mol, 
                                                   includeChirality=self.include_chirality)

class CompoundSplit(object):
    
    def __init__(self, train_size, seed):
        
        self.train_size = train_size
        self.valid_size = 1.0 - self.train_size
        self.seed = seed
    
    def generate_scaffold(self, smiles, include_chirality=False):

        mol = Chem.MolFromSmiles(smiles)
        engine = ScaffoldGenerator(include_chirality=include_chirality)
        scaffold = engine.get_scaffold(mol)
        
        return scaffold
    
    def split(self, X, y, stratify):
        
        raise NotImplementedError

class ScaffoldSplit(CompoundSplit):
    
    def __init__(self, train_size, seed):
        super(ScaffoldSplit, self).__init__(train_size, seed)
    
    def split(self, smiles, X, y, stratify):
        """
        Function to actually split dataset into training and validation sets.
        """
        
        scaffolds = dict()
        data_len = X.shape[0]
        
        for idx, smi in enumerate(smiles.tolist()):

            scaffold = self.generate_scaffold(smi)
            if scaffold not in scaffolds:
                scaffolds[scaffold] = [idx]
            else:
                scaffolds[scaffold].append(idx)
        
        # Sort scaffolds from largest to smallest
        scaffolds = {key: sorted(val) for key, val in scaffolds.items()}
        scaffold_sets = [
            scaffold_set for (scaffold, scaffold_set) in sorted(
                scaffolds.items(), key=lambda x: (len(x[1]), x[1][0]), reverse=True)
        ]
        
        train_cutoff = self.train_size * data_len
        valid_cutoff = (self.train_size + self.valid_size) * data_len
        
        train_idxs, valid_idxs = [], []
        for scaffold_set in scaffold_sets:
            
            if len(train_idxs) + len(scaffold_set) > train_cutoff:
                valid_idxs += scaffold_set
            else:
                train_idxs += scaffold_set
        
        return train_idxs, valid_idxs
