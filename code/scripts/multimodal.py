path_camda = '../../data/camda19/'

import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler
import torch.nn as nn
import torch.optim as optim
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score, matthews_corrcoef, classification_report

from imblearn.over_sampling import SMOTENC

import pandas as pd
import feather
import numpy as np

import matplotlib.pyplot as plt

from rdkit import Chem

def main():

    ####################################################################################################

    # CMaP data
    expr = feather.read_dataframe(path_camda + 'expr/data_ml_lm')
    expr = expr[expr.vDili != -1]
    expr.reset_index(inplace=True, drop=True)

    # DILIrank
    dili = pd.read_excel('../../data/DILIrank/DILIrank.xls')
    dili = dili[['Compound Name', 'vDILIConcern', 'SMILES']]
    dili.columns = ['compound', 'label', 'smiles']
    dili = dili[dili['smiles'] != '.']
    for idx in dili.index:
        smi = dili.at[idx, 'smiles']
        dili.at[idx, 'smiles'] = Chem.MolToSmiles(Chem.MolFromSmiles(smi))

    dili.reset_index(inplace=True, drop=True)

    # Merge labels from DILIrank
    expr = pd.merge(expr, dili[['compound', 'label', 'smiles']])
    labs = expr['label']
    smiles = expr['smiles']
    expr.drop(['vDili', 'label', 'smiles'], axis=1, inplace=True)
    expr.insert(2, 'smiles', smiles)
    expr.insert(3, 'label', labs)

    # Encode labels
    le = LabelEncoder()
    labs = le.fit_transform(expr['label'])
    expr['label'] = labs
    print(le.inverse_transform([0, 1, 2, 3]))

    # Add info time and dose
    info = expr['full_id'].str.split(':').tolist()
    time = [el[0].split('_')[2] for el in info]
    dose = [el[2] for el in info]

    expr.insert(3, 'time', time)
    expr.insert(4, 'dose', dose)

    # Filter time, dose, cell line and label
    time = ['6H']
    dose = ['10']
    expr = expr[((expr['time'].isin(time)) & (expr['dose'].isin(dose)) & (expr['label'].isin([1, 2, 3])))]
    expr['label'] = expr['label'] - 1

    expr.drop(['full_id', 'compound', 'time', 'dose', 'cell_line'], axis=1, inplace=True)

    threshold = 70
    expr = expr[expr['smiles'].apply(lambda x: len(x) <= threshold)]

    expr.reset_index(inplace=True, drop=True)

    ####################################################################################################

    # Split by compound
    train_comp, val_comp = train_test_split(expr.drop_duplicates('smiles'), 
                                            stratify=expr.drop_duplicates('smiles')['label'], 
                                            shuffle=True)
    train = expr[expr['smiles'].isin(train_comp['smiles'].tolist())]
    val = expr[expr['smiles'].isin(val_comp['smiles'].tolist())]
    val = val.drop_duplicates('smiles').copy()
    train.reset_index(inplace=True, drop=True)
    val.reset_index(inplace=True, drop=True)
    
    # Replicate training compounds minority class (most-DILI)
    train = pd.concat([train, train[train['label'] == 1]*2], ignore_index=True)
    train.reset_index(inplace=True, drop=True)
    
    print(f'Number of unique compounds (train): {len(train["smiles"].unique())}')
    print(f'Number of unique compounds (val): {len(val["smiles"].unique())}')

    # Encoding SMILES for RNN models
    train_charset = ''.join(train['smiles'].tolist())
    val_charset = ''.join(val['smiles'].tolist())
    charset = set(train_charset + val_charset)
    char_to_int = {c: i for i, c in enumerate(charset, 1)}

    def encode(smi, pad):
        encoded = np.zeros((pad+1), dtype=np.int8)
        for i, char in enumerate(smi):
            encoded[i] = char_to_int[char]

        return encoded

    pad_train = len(max(train['smiles'].tolist(), key=len))
    pad_val = len(max(val['smiles'].tolist(), key=len))
    pad = max(pad_train, pad_val)

    train.reset_index(inplace=True, drop=True)
    for idx in train.index:
        smi = train.at[idx, 'smiles']
        encoded_smi = encode(smi, pad)
        train.at[idx, 'smiles'] = encoded_smi

    val.drop_duplicates('smiles', inplace=True)
    val.reset_index(inplace=True, drop=True)
    for idx in val.index:
        smi = val.at[idx, 'smiles']
        encoded_smi = encode(smi, pad)
        val.at[idx, 'smiles'] = encoded_smi

    ####################################################################################################

    # PyTorch dataset
    class CamdaDataset(Dataset):

        def __init__(self, df, coln_genes: int):
            super(CamdaDataset, self).__init__()

            self.df = df
            self.coln_genes = coln_genes

        def __getitem__(self, idx):
            smi = self.df.at[idx, 'smiles']
            genes = self.df.iloc[idx, self.coln_genes:].values
            label = self.df.at[idx, 'label']

            return smi, 1, label.item()

        def __len__(self):
            return len(self.df)

    # PyTorch dataloaders
    batch_size = 128

    train = train.sample(frac=1.0)
    train.reset_index(inplace=True, drop=True)
    print(f'Train: {train.shape}')
    val = val.sample(frac=1.0)
    val.reset_index(inplace=True, drop=True)
    print(f'Validation: {val.shape}')

    train_set = CamdaDataset(train, coln_genes=2)
    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True)

    val_set = CamdaDataset(val, coln_genes=2)
    val_loader = DataLoader(val_set, batch_size=batch_size, shuffle=True)

    ####################################################################################################

    class Modal(nn.Module):

        def __init__(self, input_dim, embedding_dim, hidden_dim, 
                     num_classes):
            super(Modal, self).__init__()

            self.n_layers = 3
            self.hidden_size = hidden_dim

            self.embedding = nn.Embedding(input_dim, embedding_dim)

            self.encoder = nn.RNN(embedding_dim, hidden_dim, 
                                  num_layers=self.n_layers, dropout=0.5, 
                                  batch_first=True)

            self.classify = nn.Linear(hidden_dim, num_classes)

        def forward(self, smi, genes, hidden):
            smi = self.embedding(smi.long())

            #smi, hidden = self.encoder(smi, hidden)
            smi, hidden = self.encoder(smi)

            out = self.classify(smi[:, -1, :])

            return out, hidden

        def init_weights(self):
            nn.init.xavier_uniform_(self.embedding.weight)

            nn.init.xavier_uniform_(self.classify.weight)
            nn.init.constant_(self.classify.bias, 0)

            for name, param in self.encoder.named_parameters():
                if 'weight' in name:
                    nn.init.orthogonal_(param)

                elif 'bias' in name:
                    nn.init.constant_(param, 0)
                    r_gate = param[int(0.25 * len(param)):int(0.5 * len(param))]
                    nn.init.constant_(r_gate, 1)

        def init_hidden(self, bsz, device):
            return (torch.zeros(self.n_layers, bsz, self.hidden_size).to(device), 
                    torch.zeros(self.n_layers, bsz, self.hidden_size).to(device))

    model = Modal(input_dim=pad, embedding_dim=16, hidden_dim=64, num_classes=3)
    model.to(device)

    ####################################################################################################

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.RMSprop(model.parameters())

    def compute_accuracy(y, y_hat):        
        labels_hat = torch.argmax(y_hat, dim=1)
        train_acc = torch.sum(y.long() == labels_hat).item() / (len(y) * 1.0)

        return train_acc

    # Training
    num_epochs = 70
    training_loss = []
    val_loss = []

    print('\n')
    for epoch in range(1, num_epochs+1):
        print(f'EPOCH: {epoch}...')

        model.train()

        avg_loss = 0.0
        for idx, batch in enumerate(train_loader):
            smiles, genes, labels = batch[0].to(device), batch[1].to(device), batch[2].to(device)

            # Fit
            optimizer.zero_grad()

            hidden = model.init_hidden(smiles.size(0), device)
            out, hidden = model(smiles, genes, hidden)

            loss = criterion(out, labels.long())
            avg_loss =+ loss.item() * smiles.size(0)
            loss.backward()

            optimizer.step()

        training_loss.append(avg_loss / len(train_loader))

        # Validation
        model.eval()
        with torch.no_grad():

            avg_loss = 0.0
            y_pred = []
            y_val = []
            for idx, batch in enumerate(val_loader):
                smiles, genes, labels = batch[0].to(device), batch[1].to(device), batch[2].to(device)

                hidden = model.init_hidden(smiles.size(0), 'cpu')
                out, hidden = model(smiles, genes, hidden)
                y_val.extend(list(labels.detach().numpy()))
                y_pred.extend(list(torch.argmax(out, dim=1).detach().numpy()))

                loss = criterion(out, labels.long())
                avg_loss =+ loss.item() * smiles.size(0)

        val_loss.append(avg_loss / len(val_loader))

    # Performance
    print('\n')
    print(np.round(matthews_corrcoef(y_val, y_pred), 3))
    print(classification_report(y_val, y_pred))
    print('\n')
    
    # Plot losses
    fig, ax = plt.subplots(1, 2, figsize=(20, 7))

    ax[0].plot(range(len(training_loss)), training_loss, '-', label='training')
    ax[0].plot(range(len(val_loss)), val_loss, '-', label='validation')
    ax[0].set_xlabel('epoch')
    ax[0].set_ylabel('loss')
    ax[0].legend()

    ax[1].plot(range(len(training_loss)), training_loss, '-', label='training')
    ax[1].set_xlabel('epoch')
    ax[1].set_ylabel('loss')
    ax[1].legend()

    plt.show()
