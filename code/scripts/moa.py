"""
Helper functions for quick prototype of MOAs integration 
for DILI prediction.
"""

import os
import zipfile

import pandas as pd

from rdkit import Chem

def aggregate_tox21_assays(parent_path):
    """
    Aggregate in single file all Tox21 assays.
    """

    # Store all DataFrames to be merged
    all_dfs = []

    # Iterate over zip files
    for filename in os.listdir(parent_path):

        if filename.endswith('.zip'):
            archive = zipfile.ZipFile(os.path.join(parent_path, filename), 'r')

            for filename_in_zip in archive.namelist():

                # Get aggregated info
                if filename_in_zip.endswith('aggregrated.txt'):

                    assay = pd.read_csv(archive.open(filename_in_zip), sep='\t', index_col=False)
                    assay.drop_duplicates(subset='SMILES', inplace=True)
                    assay.dropna(axis=0, subset=['ASSAY_OUTCOME'], inplace=True)
                    assay['ASSAY_OUTCOME'] = assay['ASSAY_OUTCOME'].astype(str)
                    assay = assay[~assay.ASSAY_OUTCOME.str.startswith('inconclusive')]
                    assay.replace(to_replace='active agonist', value='active', inplace=True)
                    assay.replace(to_replace='active antagonist', value='active', inplace=True)
                    assay['PUBCHEM_CID'] = assay['PUBCHEM_CID'].fillna(-999)
                    assay['PUBCHEM_CID'] = assay['PUBCHEM_CID'].astype(int)
                    assay = assay[['PROTOCOL_NAME', 'ASSAY_OUTCOME', 'SAMPLE_NAME', 'SMILES']]

                    all_dfs.append(assay)
    
    # Concatenate DataFrames
    data = pd.concat(all_dfs)

    data = canonicalize_smiles(data, 'SMILES')

    data.reset_index(inplace=True, drop=True)
    print(f'Shape of concatenated dataset: {data.shape}.')

    return data

def encode_labels(data, coln, which='tox21'):
    """
    Encodes labels dataset.
    """

    if which == 'tox21':
        data = data.replace({coln: {'active': 1, 'inactive': 0}})
    elif which == 'dili':
        data = data.replace({coln: {'vMost-DILI-Concern': 2, 'vLess-DILI-Concern': 1, 'vNo-DILI-Concern': 0}})
    
    return data

def canonicalize_smiles(data, coln):
    """
    Creates canonical SMILES.
    """

    data.reset_index(inplace=True, drop=True)
    to_drop = []

    for idx in data.index:
        smi = data.at[idx, coln]
        try:
            new_smi = Chem.MolToSmiles(Chem.MolFromSmiles(smi))
            data.at[idx, coln] = new_smi
        except:
            to_drop.append(idx)
            continue
    
    # Drop rows for which no canonical SMILES could be produced
    data.drop(to_drop, inplace=True)
    
    return data

def retrieve_info_clue():
    
    import seaborn as sns
    import matplotlib.pyplot as plt
    import pandas as pd
    import requests
    from sklearn.preprocessing import LabelEncoder
    
    header = {'user_key': '6f9b08c5c4d2b74ace585faa81f9523e', 
              'Accept': 'application/json'}
    
    dili = pd.read_excel('../../data/DILIrank/DILIrank.xls')
    dili = dili[dili.SMILES != '.']
    dili = dili[dili.vDILIConcern != 'Ambiguous DILI-concern']
    
    not_found = 0
    res_all = {}
    
    for compound in dili['Compound Name'].tolist():
        compound = compound.lower()
        
        req = requests.get(f'https://api.clue.io/api/perts?filter=%7B%22where%22%3A%7B%22pert_iname%22%3A%22{compound}%22%7D%7D', 
                          headers=header)
        
        try:
            json = req.json()[0]
            res_all[compound] = {'moa': json['moa'], 'target': json['target']}
        except:
            not_found += 1
    
    clue = pd.DataFrame.from_dict(res_all, orient='index')
    clue.reset_index(inplace=True)
    clue.columns = ['compound', 'moa', 'target']
    
    tmp = [item for sublist in clue.target.values for item in sublist]
    all_targets = list(set(tmp))
    
    ## Create single DataFrame with DILI labels and targets as columns (one-hot encoding)
    for idx in clue.index:
        targets = clue.at[idx, 'target']
        
        for target in targets:
            clue.at[idx, target] = 1
    
    clue = pd.merge(clue, dili[['Compound Name', 'vDILIConcern', 'SMILES']], 
                    left_on='compound', right_on='Compound Name')
    clue.drop(['compound', 'moa', 'target', 'Compound Name'], 
              axis=1, inplace=True)
    clue.fillna(0, inplace=True)
    
    # Encode DILI labels
    le = LabelEncoder()
    new_labels = le.fit_transform(clue['vDILIConcern'])
    clue['vDILIConcern'] = new_labels
    
    # Drop columns with zero variance
    clue = clue.loc[:, clue.sum(axis=0) != 0.0]
    print(f'Shape of new DataFrame: {clue.shape}.')
    
    return dili, clue, all_targets

def generate_top_fingerprints(smiles: str, type_fps: str):
    """
    Generates topological fingerprints (one type 
    among those that are available) for one 
    compound (as SMILES code).
    """
    
    import rdkit.Chem
    from rdkit.Chem import AllChem
        
    assert type_fps in ['morgan', 'maccs', 'obabel']
        
    # Morgan
    radius = 5
    n_bits_morgan = 2048
    # OpenBabel
    ob_type = 'FP4'
    assert ob_type in ['FP2', 'FP3', 'FP4']
        
    molecule = rdkit.Chem.MolFromSmiles(smiles)
        
    if type_fps == 'morgan':
        fps = AllChem.GetMorganFingerprintAsBitVect(molecule, 
                                                    radius, n_bits_morgan)
        fps = list(fps)
            
        assert len(fps) == n_bits_morgan
        
    elif type_fps == 'maccs':
        fps = MACCSkeys.GenMACCSKeys(molecule)
        fps = list(fps)[1:]
            
        assert len(fps) == 166
        
    elif type_fps == 'obabel':
        molecule_ob = pybel.readstring('smi', smiles)
            
        fps = molecule_ob.calcfp(ob_type)
        tmp = np.zeros(max(fps.bits)+1)
        tmp[fps.bits] = 1
        fps = list(tmp)
        
    return fps
