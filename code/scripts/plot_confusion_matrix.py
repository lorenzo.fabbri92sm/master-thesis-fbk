import matplotlib.pyplot as plt
import seaborn as sns

import numpy as np

import sklearn.utils.multiclass as mc

def plot_confusion_matrix(cm, classes, path_to_file=None):
    """
    Plot confusion matrix. Adapted from function to plot confusion matrices 
    from Scikit-learn.
    """

    fig, ax = plt.subplots(1, 2, figsize=(12, 5))

    # CM w/ counts
    im = ax[0].imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)

    ax[0].set(xticks=np.arange(cm.shape[1]), 
            yticks=np.arange(cm.shape[0]), 
            xticklabels=classes, yticklabels=classes, 
            ylabel='true', xlabel='predicted')
    
    plt.setp(ax[0].get_xticklabels(), rotation=45, ha='right', 
            rotation_mode='anchor')
    
    fmt = '.2f'
    threshold = cm.max() / 2.0
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax[0].text(j, i, format(cm[i, j], fmt), 
                    ha='center', va='center', 
                    color='white' if cm[i, j]>threshold else 'black')
    
    ax[0].set_title('Mean Confusion Matrix for Test Sets (counts).')
    fig.tight_layout()
    
    # CM w/ probabilities
    probs = cm / cm.sum(axis=1).reshape(-1, 1)
    im = ax[1].imshow(probs, interpolation='nearest', cmap=plt.cm.Blues)

    ax[1].set(xticks=np.arange(probs.shape[1]), 
            yticks=np.arange(probs.shape[0]), 
            xticklabels=classes, yticklabels=classes, 
            ylabel='true', xlabel='predicted')
    
    plt.setp(ax[1].get_xticklabels(), rotation=45, ha='right', 
            rotation_mode='anchor')
    
    fmt = '.2f'
    threshold = probs.max() / 2.0
    for i in range(probs.shape[0]):
        for j in range(probs.shape[1]):
            ax[1].text(j, i, format(probs[i, j], fmt), 
                    ha='center', va='center', 
                    color='white' if probs[i, j]>threshold else 'black')
    
    ax[1].set_title('Mean Confusion Matrix for Test Sets (probabilities).')
    fig.tight_layout()

    # Save image to file
    if path_to_file is not None:
        fig.savefig(path_to_file, dpi=300)

    return ax
